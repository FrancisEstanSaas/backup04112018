//
//  DestinationAccountTableViewCell.swift
//  GBA
//
//  Created by Gladys Prado on 30/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class DestinationAccountTableViewCell: UITableViewCell {
    
    //MARK: Properties for Account Selection
    @IBOutlet weak var imgCurrencyFlag: UIImageView!
    @IBOutlet weak var lblCurrencyName: UILabel!
    @IBOutlet weak var lblAccountNumber: UILabel!
    @IBOutlet weak var lblAvailableBalance: UILabel!
    
    var country: [Countries] = [Countries]()
    
    var wallet: Wallet?{
        didSet{
            guard let _wallet = self.wallet else {
                return
            }
            
            let walletName = String(describing: "\(_wallet.currency) Wallet")
            let accountNumber = "**** **** **** 1234"
            let roundedWalletBalance = String(format: "%.2f", _wallet.balance)
            
            self.lblCurrencyName.text = walletName
            self.lblAccountNumber.text = accountNumber
            self.lblAvailableBalance.text = "\(_wallet.currency) \(roundedWalletBalance)"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension DestinationAccountTableViewCell{
    @discardableResult
    func set(wallet: Wallet)->Self{
        self.wallet = wallet
        return self
    }
}
