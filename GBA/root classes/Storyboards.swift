//
//  Storyboards.swift
//  ArchitectureLayout
//
//  Created by Emmanuel Albania on 10/17/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

enum Storyboards{
    var module: Module {
        get{
            var name: String = ""
            var viewControllerIdentifier: ViewControllerIdentifier? = nil
            
            switch self {
            case let .Entry(activity):
                name = "Entry"
                viewControllerIdentifier = activity.rawValue
            case let .Dashboard(activity):
                name = "Dashboard"
                viewControllerIdentifier = activity.rawValue
            case let .TransactionHistory(activity):
                name = "TransactionHistory"
                viewControllerIdentifier = activity.rawValue
            case let .ManagePayees(activity):
                name = "ManagePayees"
                viewControllerIdentifier = activity.rawValue
            case let .Accounts(activity):
                name = "Accounts"
                viewControllerIdentifier = activity.rawValue
            case let .Reload(activity):
                name = "Reload"
                viewControllerIdentifier = activity.rawValue
            case let .Transfers(activity):
                name = "Transfers"
                viewControllerIdentifier = activity.rawValue
            case let .Settings(activity):
                name = "Settings"
                viewControllerIdentifier = activity.rawValue
            case let .KYC(activity):
                name = "KYC"
                viewControllerIdentifier = activity.rawValue
            case let .Notifications(activity):
                name = "Notifications"
                viewControllerIdentifier = activity.rawValue
            }
            
            return Module(name: name, activity: viewControllerIdentifier)
        }
    }
    
    case Entry(EntryActivities)
    case Dashboard(DashboardActivities)
    case TransactionHistory(TransactionHistoryActivities)
    case ManagePayees(ManagePayeesActivities)
    case Accounts(AccountsActivities)
    case Reload(ReloadActivities)
    case Transfers(TransfersActivities)
    case Settings(SettingsActivities)
    case KYC(KYCActivities)
    case Notifications(NotificationsActivities)
}
