//
//  ViewContract.swift
//  ArchitectureLayout
//
//  Created by Emmanuel Albania on 10/13/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import UIKit
import FontAwesome_swift

protocol ViewInterface: class {
    
}


extension ViewInterface{
    
}

class RootView: ViewInterface{
    
}

class RootViewController: UIViewController, ViewInterface, UIPopoverPresentationControllerDelegate{
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    private(set) var _presenter: RootPresenter?
    var focusdObjectFrame: CGRect? = nil
    private var focusedObjectMoved = false
    
    func set(presenter: RootPresenter){
        self._presenter = presenter
    }
    
    override func loadView() {
        super.loadView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        
        self.view.isUserInteractionEnabled = true
        if self is GBAFocusableInputViewDelegate{
            guard let _ = self as? AtmFinderViewController else{
                let viewTapped = UITapGestureRecognizer(target: self, action: #selector(view_tapped(_:)))
                self.view.addGestureRecognizer(viewTapped)
                return
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton()
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if self.focusdObjectFrame == nil { return }
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            guard let objectFrame = focusdObjectFrame else { return }
            
            let objectBottomLocation = objectFrame.origin.y + objectFrame.size.height
            let keyboardYPosition = UIScreen.main.bounds.size.height - keyboardSize.height
            
            if let scrollView = self.view.subviews.first as? UIScrollView{
                UIView.animate(withDuration: 0.5, animations: {
                    scrollView.contentInset = UIEdgeInsets(top: 0,
                                                           left: 0,
                                                           bottom: keyboardSize.height + (1.6 * (self.navigationController?.navigationBar.height ?? 0)),
                                                           right: 0)
                }, completion:{
                    _ in
                    self.focusedObjectMoved = true
                })
                
            }else if self.view.frame.origin.y == 0 && objectBottomLocation > keyboardYPosition{
                UIView.animate(withDuration: 0.5, animations: {
                    self.view.frame.origin.y = (keyboardYPosition - objectBottomLocation)
                    return
                }, completion:{
                    _ in
                    self.focusedObjectMoved = true
                })
            }
            
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if self.focusdObjectFrame == nil { return }
        if !self.focusedObjectMoved { return }
        self.focusdObjectFrame = nil
        if let scrollView = self.view.subviews.first as? UIScrollView{
            UIView.animate(withDuration: 0.5, animations: {
                scrollView.contentInset = UIEdgeInsets.zero
            }, completion:{
                _ in
                self.focusedObjectMoved = false
            })
        }else if self.view.frame.origin.y != 0{
            UIView.animate(withDuration: 0.5, animations: {
                self.view.frame.origin.y = self.navigationController?.navigationBar.height ?? 0
            }, completion:{
                _ in
                self.focusedObjectMoved = false
            })
        }
    }
    
    open func addBackButton(){
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .normal)
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .highlighted)
    }
    
    @objc open func applicationDidBecomeActive(){
        print("something")
    }
    
    @objc private func view_tapped(_ sender: UIGestureRecognizer){
        self.view.resignFirstResponder()
    }
    
    @objc open func backBtn_tapped(){
        self.navigationController?.popViewController(animated: true)
    }
    
}



