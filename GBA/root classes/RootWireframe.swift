//
//  BaseWireframe.swift
//  ArchitectureLayout
//
//  Created by Emmanuel Albania on 10/13/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import UIKit


enum Transition{
    case present
    case root
    case push
}


protocol WireframeInterface: class{
    func popFromViewController(_ animated: Bool)
    func popToRootViewController(_ animated: Bool)
    func dismiss(_ animated: Bool)
}


// -Wireframe for the whole application

class RootWireframe{
    internal private(set) var _target: Storyboards?
    internal private(set) var _presenter: RootPresenter?
    
    var navigator: UINavigationController!
    
    init(_ navigator: UINavigationController) { self.navigator = navigator }
    
    func show(with transition: Transition, animated: Bool){
        guard let module = _target?.module,
            let viewController = module.activity?.viewController,
            let presenter = self._presenter else { return }
        viewController.set(presenter: presenter)
        
        switch transition {
        case .present:
            navigator.present(viewController, animated: animated, completion: nil)
        case .push:
            navigator.pushViewController(viewController, animated: animated)
        case .root:
            navigator.setViewControllers([viewController], animated: animated)
        }
    }
    
    func set(target: Storyboards){
        self._target = target
    }
    
    func set(presenter: RootPresenter?){
        self._presenter = presenter
    }
}

extension RootWireframe: WireframeInterface{
    
    func popFromViewController(_ animated: Bool) {
        navigator.popViewController(animated: animated)
    }
    
    func dismiss(_ animated: Bool) {
        navigator.dismiss(animated: animated)
    }
    
    func popToRootViewController(_ animated: Bool) {
        navigator.popToRootViewController(animated: animated)
    }
}













     
