//
//  AuthenticationEnums.swift
//  GBA
//
//  Created by Emmanuel ALbania on 15/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

class GBAAuth{
    enum TokenType: String{
        case Bearer = "Bearer"
    }
    
    enum DeviceType: Int{
        case unknown    = 0
        case Android    = 1
        case iOS        = 2
        
        var prefixValue: String{ return "GBA0\(self.rawValue)" }
    }
    
    enum AuthenticationLevel: Int{
        case oneWay     = 1
        case twoWay     = 2
    }
}

