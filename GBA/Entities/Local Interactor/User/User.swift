//
//  User.swift
//  GBA
//
//  Created by Republisys on 24/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import RealmSwift 

class User: Object{
    @objc dynamic var id: Int = 0
    @objc dynamic var firstname: String = ""
    @objc dynamic var lastname: String = ""
    @objc dynamic var mobile: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var country_id: String = ""
    @objc dynamic var currency: String?
    @objc dynamic var auth_type: String = ""
    @objc dynamic var auth_level: Int = 0
    @objc dynamic var image_id: String?
    @objc dynamic var status: Int = 0
    @objc dynamic var wallet: Wallet?
    @objc dynamic var profile_image: String?
    
    convenience init(json: JSON){
        self.init()
        print(json)
        guard let id = json["id"] as? Int,
            let firstname = json["firstname"] as? String,
            let lastname = json["lastname"] as? String,
            let mobile = json["mobile"] as? String,
            let email = json["email"] as? String,
            let country_id = json["country_id"] as? String,
            let auth_type = json["auth_type"] as? String,
            let auth_level = json["auth_level"] as? Int,
            let status = json["status"] as? Int else{
                fatalError("Parsing json to entity gailed failed in User.swift ")
        }
        
        let profile_image = json["profile_image"] as? String
        let image_id = json["image_id"] as? String
        let currency = json["currency"] as? String
        
        if let walletData = json["wallet"] as? [String: Any]{
            let wallet = Wallet(json: walletData)
            self.wallet = wallet
        }
        
        self.id = id
        self.firstname = firstname
        self.lastname = lastname
        self.mobile = mobile
        self.email = email
        self.country_id = country_id
        self.currency = currency
        self.auth_type = auth_type
        self.auth_level = auth_level
        self.image_id = image_id
        self.status = status
        self.profile_image = profile_image
        
        print(self)
    }
    
    override static func primaryKey()->String?{
        return "id"
    }
}

extension User{
    var Fullname: String{
        get{ return "\(self.firstname) \(self.lastname)" }
    }
}

extension User: LocalEntityProcess{
    
}











