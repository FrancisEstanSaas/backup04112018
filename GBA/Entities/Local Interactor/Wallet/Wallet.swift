//
//  Wallet.swift
//  GBA
//
//  Created by Republisys on 24/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import RealmSwift

class Wallet: Object{
    @objc dynamic var id: Int = 0
    @objc dynamic var currency: String = ""
    @objc dynamic var is_default: Int = 0
    @objc dynamic var reference: String = ""
    @objc dynamic var status: Int = 0
    @objc dynamic var created_at: String = ""
    @objc dynamic var balance: Double = 0.00
    
    convenience init(json: JSON){
        self.init()
        print(json)
        guard let id = json["id"] as? Int,
            let currency = json["currency"] as? String,
            let is_default = json["is_default"] as? Int,
            let reference = json["reference"] as? String,
            let status = json["status"] as? Int,
            let created_at = json["created_at"] as? String else{
                fatalError("Error in parsing data for Wallet")
        }
        
        if let balance = json["balance"] as? Double{
            self.balance = balance
        }
        
        self.id = id
        self.currency = currency
        self.is_default = is_default
        self.reference = reference
        self.status = status
        self.created_at = created_at
    }
    
    override static func primaryKey()->String?{
        return "id"
    }
}

extension Wallet: LocalEntityProcess{
    
}
