//
//  Payee.swift
//  GBA
//
//  Created by Republisys on 22/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import RealmSwift
import CryptoSwift

class Payee: Object{
    @objc dynamic var uid: Int = 0
    @objc dynamic var nickname: String?
    @objc dynamic var firstname: String?
    @objc dynamic var lastname: String?
    @objc dynamic var mobile: String?
    @objc dynamic var email: String?
    @objc dynamic var image_id: String? = ""
    @objc dynamic var created_at: String?
    @objc dynamic var status: Int = 0
    @objc dynamic var currency: String? = ""
    @objc dynamic var country_id: String?

    fileprivate var _status: PayeeStatus = .inactive{
        didSet{ self.status = _status.rawValue }
    }
    
    convenience init(json: JSON) {
        self.init()

        guard let uid = (json["uid"] as? Int) ?? (json["id"] as? Int),
            let firstname = json["firstname"] as? String,
            let lastname = json["lastname"] as? String,
            let mobile = json["mobile"] as? String,
            let email = json["email"] as? String,
            let status = json["status"] as? Int,
            let nickname = json["nickname"] as? String,
            let country_id = json["country_id"] as? String else{
                    fatalError("Error in parsing data from Payee")
        }
        
        self.uid = uid
        //self.nickname = json["nickname"] as? String ?? ""
        self.nickname = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: nickname) //firstname
        self.firstname = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: firstname) //firstname
        self.lastname = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: lastname) //lastname
        self.mobile = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: mobile) //mobile
        self.email = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: email) //email
        self.image_id = json["image_id"] as? String
        self.created_at = json["created_at"] as? String ?? ""
        self._status = PayeeStatus(rawValue: status)!
        self.currency = json["currency"] as? String ?? ""
        self.country_id = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: country_id) //country_id
    }
    
    override static func primaryKey()->String?{
        return "uid"
    }
}

//Functions
extension Payee{
    var fullname: String{
        get {
            let fname = self.firstname ?? ""
            let lname = self.lastname ?? ""
            return "\(fname)\((fname == "" ? "": " "))\(lname)"
        }
    }
    
    func encryptUserData(key: String, iv: String, userData: String) -> String {
        let data = userData.data(using: .utf8)!
        let encrypted = try! AES(key: key, iv: iv).encrypt([UInt8](data))
        let encryptedData = Data(encrypted)
        return encryptedData.base64EncodedString()
    }
    
}

extension Payee: LocalEntityProcess{
    func get(by id: Int)->Payee?{
        let predicate = NSPredicate(format: "uid == %@", id)
        return self.search(by: predicate).first
    }
}
