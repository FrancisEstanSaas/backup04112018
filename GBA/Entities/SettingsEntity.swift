//
//  SettingsEntity.swift
//  GBA
//
//  Created by Gladys Prado on 22/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

struct ChangePasswordEntity: Decodable{
    var old_password: String? = ""
    var password: String? = ""
    var password_confirm: String? = ""
    
    init(old_password: String, password: String, password_confirm: String){
        self.old_password = old_password
        self.password =  password
        self.password_confirm = password_confirm
    }
}

struct ChangeProfileDetailsEntity: Decodable{
    var firstname: String? = nil
    var lastname: String? = nil
    
    init(firstname: String, lastname: String, country_id: String, email: String){
        self.firstname = firstname
        self.lastname = lastname
    }
}
