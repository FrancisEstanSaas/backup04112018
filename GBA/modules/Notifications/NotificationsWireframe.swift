//
//  NotificationsWireframe.swift
//  GBA
//
//  Created by Gladys Prado on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

class NotificationsWireframe: RootWireframe{
    
    
    func configureModule(activity: NotificationsActivities){
        guard let target =  self._target else{
            fatalError("[NotificationsWireframe: configureModule] target not found")
        }
        
        if _presenter == nil{
            guard let presenter = activity.getPresenter(with: _target?.module.activity?.viewController as! NotificationsViewController, and: self) as? NotificationsRootPresenter else{
                fatalError("declared presenter cannot be found in Notifications module")
            }
            
            self.set(presenter: presenter)
        }else{
            guard let presenter = _presenter as? NotificationsRootPresenter else{
                fatalError("declared presenter cannot be parsed for Notifications module")
            }
            
            presenter.view = target.module.activity!.viewController as! NotificationsViewController
            self.set(presenter: presenter)
        }
    }
    
    func show(`from` activity: NotificationsActivities, with transition: Transition, animated: Bool = true){
        self.set(target: .Notifications(activity))
        self.configureModule(activity: activity)
        self.show(with: transition, animated: animated)
    }
}

extension NotificationsWireframe{
    
    func navigate(to activity: NotificationsActivities, with presenter: RootPresenter? = nil){
        var transition: Transition = .present
        
        self.set(presenter: presenter)
        
        switch activity {
        case .NotificationsListView:
            transition = .root
//        case .NotificationDetailsView:
//            transition = .push
        }
        self.show(from: activity, with: transition, animated: true)
    }
    
    func getViewController(from activity: NotificationsActivities)->NotificationsViewController?{
        self.set(target: .Notifications(activity))
        self.configureModule(activity: activity)
        
        guard let module = _target?.module,
            let viewController = module.activity?.viewController as? NotificationsViewController,
            let presenter = self._presenter else { return nil}
        
        viewController.set(presenter: presenter)
        
        return viewController
    }
    
}
