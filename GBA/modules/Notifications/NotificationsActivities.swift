//
//  NotificationsActivities.swift
//  GBA
//
//  Created by Gladys Prado on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

enum NotificationsActivities: ViewControllerIdentifier{
    
    case NotificationsListView = "NotificationsListView"
//    case NotificationDetailsView = "NotificationDetailsView"
    
    func getPresenter(with viewController: NotificationsViewController, and wireframe: NotificationsWireframe)->RootPresenter?{
        switch self {
        case .NotificationsListView:
            return NotificationsListPresenter(wireframe: wireframe, view: viewController)
//        case .NotificationDetailsView:
//            return NotificationsListPresenter(wireframe: wireframe, view: viewController)
        }
    }
}
