//
//  ListGBAWalletsViewController.swift
//  GBA
//
//  Created by Gladys Prado on 21/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit
import CryptoSwift

class ListGBAWalletsViewController: AccountsRootViewController {
    
    @IBOutlet weak var imgWalletPic: UIImageView!
    @IBOutlet weak var lblWalletName: UILabel!
    @IBOutlet weak var lblWalletBalance: UILabel!
    
    @IBOutlet weak var btnReload: UIButton!
    @IBOutlet weak var btnViewTransactions: UIButton!
    
    @IBOutlet weak var cardImageView_container: GBACard!
    
    fileprivate var wallet: Wallet?{
        get{ return self.presenter.interactor.local.wallet }
    }
    
    fileprivate var profile: User?{
        get{ return self.presenter.interactor.local.userProfile }
    }
    
    var nav: UINavigationController{
        let _nav = UINavigationController()
        
        _nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        _nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        _nav.navigationBar.tintColor = GBAColor.white.rawValue
        _nav.isNavigationBarHidden = true
        
        return _nav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.bringSubview(toFront: self.navigationController!.navigationBar)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.btnReload.layer.borderWidth = 1
        self.btnReload.layer.borderColor = GBAColor.primaryBlueGreen.rawValue.cgColor
        self.btnReload.layer.cornerRadius = 5
        
        self.btnViewTransactions.layer.borderWidth = 1
        self.btnViewTransactions.layer.borderColor = GBAColor.primaryBlueGreen.rawValue.cgColor
        self.btnViewTransactions.layer.cornerRadius = 5
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Accounts"
        
        self.repopulateCardInfo()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.parent?.title = " "
    }
    
    func decryptUserData(key: String, iv: String, userData: String) -> String {
        let data = Data(base64Encoded: userData)!
        let decrypted = try! AES(key: key, iv: iv).decrypt([UInt8](data))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
    }
    
    private func repopulateCardInfo(){
        guard let userProfile = self.profile,
            let userWallet = self.wallet else{
                return
        }
        
        let firstName = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.firstname)
        let lastName = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.lastname)
        
        
        let userFullName = "\(firstName) \(lastName)"
        
        self.cardImageView_container
            .set(owner: userFullName)
        
        self.lblWalletName.text = "\(userWallet.currency) Wallet"
        self.lblWalletBalance.text = "\(userWallet.currency) \(userWallet.balance.toString)"
        self.cardImageView_container.layoutSubviews()
    }
    
    @IBAction func proceedToReloadModule(_ sender: UIButton) {
        
        let nav = UINavigationController()
        nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        nav.navigationBar.tintColor = GBAColor.white.rawValue
        nav.isNavigationBarHidden = false
        
        ReloadWireframe(nav).navigate(to: .ReloadOptionsView)
        
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func proceedToTransactionHistory(_ sender: UIButton) {
        
        let nav = UINavigationController()
        nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        nav.navigationBar.tintColor = GBAColor.white.rawValue
        nav.isNavigationBarHidden = false
        
        TransactionHistoryWireframe(nav).navigate(to: .TransactionHistoryscreen)
        
        self.present(nav, animated: true, completion: nil)
    }
    
    override func backBtn_tapped() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
