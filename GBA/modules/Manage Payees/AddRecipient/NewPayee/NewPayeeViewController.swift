
//
//  NewPayee.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/1/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class NewPayeeViewController: ManagePayeesModuleViewController{
    
    
    @IBOutlet weak var firstname_textField: GBATitledTextField!
    @IBOutlet weak var lastname_textField: GBATitledTextField!
    @IBOutlet weak var nickname_textField: GBATitledTextField!
    @IBOutlet weak var mobileNumber_textField: GBATitledTextField!
    @IBOutlet weak var email_textField: GBATitledTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(saveBtnTapped))
        
        firstname_textField
            .set(placeholder: "First Name")
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(next: lastname_textField)
            .set(self)
        
        lastname_textField
            .set(placeholder: "Last Name")
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(next: nickname_textField)
            .set(self)
        
        nickname_textField
            .set(placeholder: "Nickname")
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(next: email_textField)
            .set(self)
        
        email_textField
            .set(placeholder: "Email")
            .set(alignment: .left)
            .set(returnKey: .default)
            .set(self)

        mobileNumber_textField
            .set(placeholder: "Mobile")
            .set(textFieldIsEditable: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.title = "New Payee"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @objc func saveBtnTapped(){
        let name = NSAttributedString(string: "\(self.firstname_textField.text) \(self.lastname_textField.text)",
            attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: GBAText.Size.subContent.rawValue)!])
        let message = NSAttributedString(string: " was added successfully to your ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: GBAText.Size.subContent.rawValue)!])
        let payee = NSAttributedString(string: "Payee",
            attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: GBAText.Size.subContent.rawValue)!])
        
        let content = NSMutableAttributedString()
        content.append(name)
        content.append(message)
        content.append(payee)
        self.presenter.wireframe.presentSuccessPage(title: "Add Recipient", message: content)
    }
}
extension NewPayeeViewController: GBAFocusableInputViewDelegate{
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }

}
