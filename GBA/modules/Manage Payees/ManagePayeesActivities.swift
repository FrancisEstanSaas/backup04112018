//
//  ManagePayeesActivities.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/1/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

enum ManagePayeesActivities{
    case ManagePayeeListscreen
    case AddRecipientscreen
    case NewPayeescreen
    case PayeeProfilescreen
    case AddNewRecipientscreen
    case EditPayeescreen(EditPayeeMode, Payee)
    
    
    
    func getPresenter(with viewController: ManagePayeesModuleViewController, and wireframe: ManagePayeesWireframe)->RootPresenter?{

        switch self {
        case .ManagePayeeListscreen:
            return ManagePayeeListPresenter(wireframe: wireframe, view: viewController)
        case .AddRecipientscreen:
            return AddRecipientPresenter(wireframe: wireframe, view: viewController)
        case .NewPayeescreen:
            return nil
        case .PayeeProfilescreen:
            return PayeeProfilePresenter(wireframe: wireframe, view: viewController)
        case .AddNewRecipientscreen:
            return AddRecipientPresenter(wireframe: wireframe, view: viewController)
        case .EditPayeescreen(let mode, let payee):
            return EditPayeePresenter(wireframe: wireframe, view: viewController)
                .set(mode: mode)
                .set(payee: payee)
        }
    }
    
    var rawValue: ViewControllerIdentifier{
        get{
            switch self{
            case .ManagePayeeListscreen:
                return "ManagePayeeListscreen"
            case .AddRecipientscreen:
                return "AddRecipientscreen"
            case .NewPayeescreen:
                return "NewPayeescreen"
            case .PayeeProfilescreen:
                return "PayeeProfilescreen"
            case .AddNewRecipientscreen:
                return "AddNewRecipientscreen"
            case .EditPayeescreen(_):
                return "EditPayeescreen"
            }
        }
    }
}

