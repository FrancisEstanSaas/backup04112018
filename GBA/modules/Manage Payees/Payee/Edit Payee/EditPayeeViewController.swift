//
//  EditPayeeViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/4/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift
import CryptoSwift

class EditPayeeViewController: ManagePayeesModuleViewController{
    
    @IBOutlet weak var profile_imageView: UIImageView!
    @IBOutlet weak var nickname_label: UILabel!
    
    @IBOutlet weak var firstName_textField: GBATitledTextField!
    @IBOutlet weak var lastName_textField: GBATitledTextField!
    @IBOutlet weak var nickName_textField: GBATitledTextField!
    
    @IBOutlet weak var mobileNumber_textField: GBATitledTextField!
    @IBOutlet weak var email_textField: GBATitledTextField!
    @IBOutlet weak var country_dropDown: GBAPopOverButton!
    
    private var selectedCountry: Countries = .Philippines
    
    var currentPresenter: EditPayeePresenter{
        guard let prsntr = self.presenter as? EditPayeePresenter else{
            fatalError("Error in parsing presenter in EditPayeeViewController0")
        }
        return prsntr
    }
    
    private var payee: Payee{
        get{ return self.currentPresenter.getPayee() ?? Payee() }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.set(view: self)
        
        let defaultCountry = Countries.Philippines
        
        firstName_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "First name")
            .set(next: lastName_textField)
        
        lastName_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "Last name")
            .set(next: nickName_textField)
        
        nickName_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(titledFieldDelegate: self)
            .set(placeholder: "Nickname")
            .set(next: mobileNumber_textField)
        
        country_dropDown
            .set(parent: self)
            .set(viewController: GBAPopOverCountryTableview().set(delegate: self))
            .set(text: defaultCountry.name)
            .set(placeholder: "Country")
            .set(icon: UIImage(imageLiteralResourceName: defaultCountry.id))

        mobileNumber_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "Mobile")
            .set(next: email_textField)
        
        email_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .done)
            .set(placeholder: "Email")
        
        self.setEditMode()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.title = "\(self.currentPresenter.mode.rawValue) Payee"
        
        self.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    func decryptUserData(key: String, iv: String, userData: String) -> String {
        let data = Data(base64Encoded: userData)!
        let decrypted = try! AES(key: key, iv: iv).decrypt([UInt8](data))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
    }
    
    func setEditMode(){
        switch self.currentPresenter.mode {
        case .update:
            self.addSubmitButton(title: "Save")
            self.navigationItem.leftBarButtonItem? = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.backBtn_tapped))
            self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.subContent.rawValue)] , for: .normal)
            self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.subContent.rawValue)] , for: .highlighted)
            
            self.firstName_textField.set(userInteractionEnabled: false)
            self.lastName_textField.set(userInteractionEnabled: false)
            self.nickName_textField.set(userInteractionEnabled: true)
            self.country_dropDown.isUserInteractionEnabled = false
            self.mobileNumber_textField.set(userInteractionEnabled: false)
            self.email_textField.set(userInteractionEnabled: false)
            
        case .add:
            self.addSubmitButton(title: "Add")
            self.addBackButton()
            
            self.firstName_textField.set(userInteractionEnabled: false)
            self.lastName_textField.set(userInteractionEnabled: false)
            self.nickName_textField.set(userInteractionEnabled: true)
            self.country_dropDown.isUserInteractionEnabled = false
            self.mobileNumber_textField.set(userInteractionEnabled: false)
            self.email_textField.set(userInteractionEnabled: false)
            
        case .read:
            self.addBackButton()
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .listUL), style: .plain, target: self, action: #selector(setMenu))
            self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .normal)
            self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .highlighted)
            
            self.firstName_textField.set(userInteractionEnabled: false)
            self.lastName_textField.set(userInteractionEnabled: false)
            self.nickName_textField.set(userInteractionEnabled: false)
            self.country_dropDown.isUserInteractionEnabled = false
            self.mobileNumber_textField.set(userInteractionEnabled: false)
            self.email_textField.set(userInteractionEnabled: false)
            
        }
    }
    
    @objc func setMenu(){
        switch self.currentPresenter.mode{
        case .add       : break
        case .update    : break
        case .read      :
            let alert = UIAlertController(title: "Menu", message: "", preferredStyle: .actionSheet)
           
            let edit = UIAlertAction(title: "Edit Payee", style: .default, handler: { (_) in
                self.currentPresenter.set(mode: .update)
                self.setEditMode()
                _ = self.nickName_textField.becomeFirstResponder()
            })
            
            let delete = UIAlertAction(title: "Delete Payee", style: .default, handler: { (_) in
                self.currentPresenter.remove()
            })
            
            let schedule = UIAlertAction(title: "Schedule a transaction", style: .default, handler: { (_) in
                alert.dismiss(animated: true, completion: nil)
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
                alert.dismiss(animated: true, completion: nil)
            })
            
            alert.addAction(edit)
            alert.addAction(delete)
            alert.addAction(schedule)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    internal func reloadData(){
        let payee = self.payee
        
        let displayFirstName = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: payee.firstname!)
        let displayLastName = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: payee.lastname!)
        let displayNickName = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: payee.nickname ?? "")
        let displayCountryID = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: payee.country_id ?? "PH")
        let displayMobile = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: payee.mobile!)
        let displayEmail = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: payee.email!)
        
        self.firstName_textField.set(text: displayFirstName) //payee.firstname!
        self.lastName_textField.set(text: displayLastName) //payee.lastname!
        self.nickName_textField.set(text: displayNickName) //payee.nickname ?? ""
        self.nickname_label.text = (nickname_label.text != "") ? nickname_label.text: "\(firstName_textField.text) \(lastName_textField.text)"
        self.selectedCountry = Countries(country: displayCountryID) //payee.country_id ?? "PH"
        self.mobileNumber_textField.set(text: displayMobile) //payee.mobile!
        self.email_textField.set(text: displayEmail) //payee.email!
        
        
    }
    
    private func addSubmitButton(title: String){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(   title: title,
                                                                    style: .plain,
                                                                    target: self,
                                                                    action: #selector( submitBtn ) )
        
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.subContent.rawValue)] , for: .normal)
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.subContent.rawValue)] , for: .highlighted)
    }
    
    @objc private func submitBtn(){
        switch self.currentPresenter.mode{
        case .add:
            let payee = self.payee
            payee.nickname = self.nickName_textField.text
            self.currentPresenter.addRecipient(payee: payee)
        case .update:
            self.currentPresenter.update(self.nickName_textField.text)
        case .read:
            break
        }
    }
    
    override func backBtn_tapped(){
        switch self.currentPresenter.mode {
        case .update:
            let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel editing payee?", preferredStyle: .alert)
            
            let yes = UIAlertAction(title: "Yes", style: .destructive, handler: { (_) in
                self.currentPresenter.set(mode: .read)
                self.nickName_textField.set(text: self.currentPresenter.getPayee()?.nickname ?? "")
                self.setEditMode()
                alert.dismiss(animated: true, completion: nil)
            })
            
            let no = UIAlertAction(title: "No", style: .default, handler: { (_) in
                alert.dismiss(animated: true, completion: nil)
            })
            
            alert.addAction(no)
            alert.addAction(yes)
            
            self.present(alert, animated: true, completion: nil)
            
        case .add:
            let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel adding current payee?", preferredStyle: .alert)
            
            let yes = UIAlertAction(title: "Yes", style: .destructive, handler: { (_) in
                alert.dismiss(animated: true, completion: nil)
                self.navigationController?.popToRootViewController(animated: true)
            })
            
            let no = UIAlertAction(title: "No", style: .default, handler: { (_) in
                alert.dismiss(animated: true, completion: nil)
            })
            
           
            alert.addAction(no)
            alert.addAction(yes)
            
            self.present(alert, animated: true, completion: nil)
        default:
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension EditPayeeViewController: GBAFocusableInputViewDelegate{
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
    
    func didChange(value: String?) {
        self.nickname_label.text = value
    }
}

extension EditPayeeViewController:GBATitledTextFieldDelegate{
    func didChange(textField: GBATitledTextField) {
        if textField == self.nickName_textField{
            self.nickname_label.text = (textField.text != "") ? textField.text: "\(firstName_textField.text) \(lastName_textField.text)"
        }
    }
}

extension EditPayeeViewController{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension EditPayeeViewController: GBAPopOverDelegate{
    func GBAPopOver(didSelect cellView: UITableViewCell) {
        guard let cell = cellView as? GBACountryCellView else { fatalError("Error found in parsing cellView to GBACountryCellView") }
        
        country_dropDown.set(icon: cell.flagImage.image!)
        country_dropDown.set(text: cell.countryLabel.text!)
        self.selectedCountry = cell.country
    }
}

