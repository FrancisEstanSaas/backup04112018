//
//  PayeeProfileViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/4/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome_swift

class PayeeProfileViewController: ManagePayeesModuleViewController{
    
    @IBOutlet weak var profile_imageView: UIImageView!
    @IBOutlet weak var nickname_label: UILabel!
    
    @IBOutlet weak var firstName_textField: GBATitledTextField!
    @IBOutlet weak var lastName_textField: GBATitledTextField!
    @IBOutlet weak var nickName_textField: GBATitledTextField!
    
    @IBOutlet weak var mobileNumber_textField: GBATitledTextField!
    @IBOutlet weak var email_textField: GBATitledTextField!
    @IBOutlet weak var country_dropDown: GBAPopOverButton!
    
    private var selectedCountry: Countries = .Philippines
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaultCountry = Countries.Philippines
        
        let rightBarButton = UIBarButtonItem(title: String.fontAwesomeIcon(name: .ellipsisH), style: .plain, target: self, action: #selector(showMenu))
        rightBarButton.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)], for: .normal)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        profile_imageView
            .roundCorners()
        
        firstName_textField
            .set(self)
            .set(max: 1)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "First name")
            .set(next: lastName_textField)
            .set(textFieldIsEditable: false)
        
        lastName_textField
            .set(self)
            .set(max: 1)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "Last name")
            .set(next: nickName_textField)
            .set(textFieldIsEditable: false)
        
        nickName_textField
            .set(self)
            .set(max: 1)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "Nickname")
            .set(next: mobileNumber_textField)
            .set(textFieldIsEditable: false)
        
        country_dropDown
            .set(parent: self)
            .set(viewController: GBAPopOverCountryTableview().set(delegate: self))
            .set(text: defaultCountry.name)
            .set(placeholder: "Country")
            .set(icon: UIImage(imageLiteralResourceName: defaultCountry.id))

        mobileNumber_textField
            .set(self)
            .set(max: 1)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "Mobile")
            .set(next: email_textField)
            .set(textFieldIsEditable: false)

        email_textField
            .set(self)
            .set(max: 1)
            .set(alignment: .left)
            .set(returnKey: .done)
            .set(placeholder: "Email")
            .set(textFieldIsEditable: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.title = "Payee"
        
        firstName_textField
            .set(text: "Jayson")
        lastName_textField
            .set(text: "Craig")
        nickName_textField
            .set(text: "Jason")

        mobileNumber_textField
            .set(text: "+6356 1234 123")
        email_textField
            .set(text: "ej.albania@gmail.com")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.title = " "
    }
    
    @objc private func showMenu(){
        let menu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let scheduleTransaction = UIAlertAction(title: "Schedule a Transaction", style: .default) { _ in
            print()
        }
        let editPayee = UIAlertAction(title: "Edit Payee", style: .default) { _ in
            print()
        }
        let deletePayee = UIAlertAction(title: "Delete", style: .destructive) { _ in
            print()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        
        menu.addAction(scheduleTransaction)
        menu.addAction(editPayee)
        menu.addAction(deletePayee)
        menu.addAction(cancel)
        
        self.present(menu, animated: true, completion: nil)
    }
}

extension PayeeProfileViewController: GBAFocusableInputViewDelegate{
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
    
    
}

extension PayeeProfileViewController{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension PayeeProfileViewController: GBAPopOverDelegate{
    func GBAPopOver(didSelect cellView: UITableViewCell) {
        guard let cell = cellView as? GBACountryCellView else { fatalError("Error found in parsing cellView to GBACountryCellView") }
        
        country_dropDown.set(icon: cell.flagImage.image!)
        country_dropDown.set(text: cell.countryLabel.text!)
        self.selectedCountry = cell.country
    }
}
