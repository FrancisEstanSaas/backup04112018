//
//  KYCWireframe.swift
//  GBA
//
//  Created by EDI on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

class KYCWireframe: RootWireframe{

    func configureModule(activity: KYCActivities){
        guard let target =  self._target else{
            fatalError("[KYCWireframe: configureModule] target not found")
        }

        if _presenter == nil{

            guard let presenter = activity.getPresenter(with: _target?.module.activity?.viewController as! KYCModuleViewController, and: self) as? KYCRootPresenter else{
                fatalError("declared presenter cannot be found in Entry module")
            }

            self.set(presenter: presenter)
        }else{
            guard let presenter = _presenter as? KYCRootPresenter else{
                fatalError("declared presenter cannot be parsed for Entry module")
            }

            presenter.view = target.module.activity!.viewController as! KYCModuleViewController
            self.set(presenter: presenter)
        }
    }

    func show(`from` activity: KYCActivities, with transition: Transition, animated: Bool = true){
        self.set(target: .KYC(activity))
        self.configureModule(activity: activity)
        self.show(with: transition, animated: animated)
    }
}

extension KYCWireframe {
    
    func navigate(to activity: KYCActivities, with presenter: RootPresenter? = nil){
        var transition: Transition = .present
        
        self.set(presenter: presenter)
        
        switch activity {
        case .PersonalDetailsVC:
            transition = .root
        case .PersonalAddressVC:
            transition = .push
        case .PersonalSalaryVC:
            transition = .push
        case .GovtIDVC:
            transition = .push
        case .BillingInfoVC:
            transition = .push
            
        }
        self.show(from: activity, with: transition, animated: true)
    }
            
    func getViewController(from activity: KYCActivities)->KYCModuleViewController?{
        self.set(target: .KYC(activity))
        self.configureModule(activity: activity)
        
        guard let module = _target?.module,
            let viewController = module.activity?.viewController as? KYCModuleViewController,
            let presenter = self._presenter else { return nil}
        
        viewController.set(presenter: presenter)
        
        return viewController
    }
    
    
}








