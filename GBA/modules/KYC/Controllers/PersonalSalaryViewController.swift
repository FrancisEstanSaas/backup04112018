//
//  PersonalSalaryViewController.swift
//  GBA
//
//  Created by EDI on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

class PersonalSalaryViewController: KYCModuleViewController {
    
    //Labels and Checkboxes
    var currentPresenter: KYCPagePresenter{
        guard let prsntr = self.presenter as? KYCPagePresenter
            else{ fatalError("Error in parsing presenter for KYCViewController") }
        return prsntr
    }
    
    override func viewDidLoad() {
        self.addBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.title = "Registration"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .black
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(submit_tapped(_:)))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        self.view.backgroundColor = GBAColor.white.rawValue
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    override func backBtn_tapped() {
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel your registration?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil) }
    
    
    override func addBackButton() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .normal)
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .highlighted)
        
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    @objc private func submit_tapped(_ sender: UIBarButtonItem){
        self.currentPresenter.wireframe.navigate(to: .GovtIDVC)
    }
    
}

