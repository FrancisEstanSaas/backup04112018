//
//  PersonalDetailsViewController.swift
//  GBA
//
//  Created by EDI on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class PersonalDetailsViewController: KYCModuleViewController {
    
    @IBOutlet weak var firstName_textField: GBATitledTextField!
    @IBOutlet weak var lastName_textField: GBATitledTextField!
    @IBOutlet weak var birthDate: UIDatePicker!
    @IBOutlet weak var lblDateSelected: UILabel!
    @IBOutlet weak var country_dropDown: GBAPopOverButton!
    
    @IBOutlet var requiredFields: [GBATitledTextField]!
    
    private var selectedCountry: Countries = .Philippines
    
    var currentPresenter: KYCPagePresenter{
        guard let prsntr = self.presenter as? KYCPagePresenter
            else{ fatalError("Error in parsing presenter for KYCViewController") }
        return prsntr
    }
    
    override func viewDidLoad() {
         let defaultCountry = Countries.Philippines
        //set function for selecting dates
        birthDate.addTarget(self, action: #selector(datePickerChanged), for: UIControlEvents.valueChanged)
        birthDate.setValue(GBAColor.primaryBlueGreen.rawValue, forKey: "textColor")
        
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
//
        let strDate = dateFormatter.string(from: currentDate)
        self.lblDateSelected.text = strDate
//        self.dateTransfer.minimumDate = Date()
        
        self.firstName_textField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(next: lastName_textField)
            //.set(titledFieldDelegate: self as! GBATitledTextFieldDelegate )
            .set(placeholder: "First Name")
            .set(validation: .name(message: "Invalid first name"))
        
        self.lastName_textField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(next: country_dropDown)
            .set(placeholder: "Last Name")
            //.set(titledFieldDelegate: self as! GBATitledTextFieldDelegate )
            .set(validation: .name(message: "Invalid last name"))
     
        self.country_dropDown
            .set(parent: self)
            .set(placeholder: "Country")
            .set(text: defaultCountry.name)
            .set(icon: UIImage(imageLiteralResourceName: defaultCountry.id))
            .set(viewController: GBAPopOverCountryTableview().set(delegate: self))
        
        self.addBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.title = "Registration"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .black
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(submit_tapped(_:)))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        self.view.backgroundColor = GBAColor.white.rawValue
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    override func backBtn_tapped() {
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel your  KYC registration?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil) }
    
    
    override func addBackButton() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .normal)
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .highlighted)
        
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    @objc private func submit_tapped(_ sender: UIBarButtonItem){
        self.currentPresenter.wireframe.navigate(to: .PersonalAddressVC)
    }
    
    @objc func datePickerChanged(dateTransfer: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        
        let strDate = dateFormatter.string(from: dateTransfer.date)
        self.lblDateSelected.text = strDate
    }
    
    
}

extension PersonalDetailsViewController{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}


extension PersonalDetailsViewController: GBAPopOverDelegate{
    func GBAPopOver(didSelect cellView: UITableViewCell) {
        guard let cell = cellView as? GBACountryCellView else { fatalError("Error found in parsing cellView to GBACountryCellView") }
        
        country_dropDown.set(icon: cell.flagImage.image!)
        country_dropDown.set(text: cell.countryLabel.text!)
        self.selectedCountry = cell.country
        
    }
}
