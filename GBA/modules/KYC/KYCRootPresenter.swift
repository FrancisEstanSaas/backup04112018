//
//  KYCRootPresenter.swift
//  GBA
//
//  Created by EDI on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

class KYCRootPresenter: RootPresenter{
    
    var wireframe: KYCWireframe
    var view: KYCModuleViewController
    var interactor: (remote: KYCRemoteInteractor, local: RootLocalInteractor) = (KYCRemoteInteractor(),RootLocalInteractor())
    
    init(wireframe: KYCWireframe, view: KYCModuleViewController){
        self.wireframe = wireframe
        self.view = view
    }
    
    func set(view: KYCModuleViewController){
        self.view = view
    }
    
    func showAlert(with title: String?, message: String, completion: @escaping (()->Void)){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.view.present(alert, animated: true, completion: nil)
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { _ in
            alert.dismiss(animated: true, completion: completion)
        }
    }
}
