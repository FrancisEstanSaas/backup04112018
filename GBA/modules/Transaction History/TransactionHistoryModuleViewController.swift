//
//  TransactionHistoryModuleViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/29/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class TransactionHistoryModuleViewController: RootViewController{
    var presenter: TransactionHistoryRootPresenter{
        get {
            let prsntr = self._presenter as! TransactionHistoryRootPresenter
            return prsntr
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
