//
//  TransactionHistoryActivities.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/29/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

enum TransactionHistoryActivities: ViewControllerIdentifier{
    
    case TransactionHistoryscreen = "TransactionHistoryscreen"
    case TransactionDetailscreen = "TransactionDetailscreen"
    
    func getPresenter(with viewController: TransactionHistoryModuleViewController, and wireframe: TransactionHistoryWireframe)->RootPresenter?{
        switch self {
        case .TransactionHistoryscreen:
            return TransactionHistoryListPresenter(wireframe: wireframe, view: viewController)
        case .TransactionDetailscreen:
            return TransactionHistoryListPresenter(wireframe: wireframe, view: viewController)
        }
    }
}


