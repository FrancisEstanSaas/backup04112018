//
//  TransactionHistoryWireframe.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/29/17.
//  Copyright © 2017 Republisys. All rights reserved.
//


class TransactionHistoryWireframe: RootWireframe{
    
    
    func configureModule(activity: TransactionHistoryActivities){
        guard let target =  self._target else{
            fatalError("[TransactionHistoryWireframe: configureModule] target not found")
        }
        
        if _presenter == nil{
            guard let presenter = activity.getPresenter(with: _target?.module.activity?.viewController as! TransactionHistoryModuleViewController, and: self) as? TransactionHistoryRootPresenter else{
                fatalError("declared presenter cannot be found in TransactionHistory module")
            }
            
            self.set(presenter: presenter)
        }else{
            guard let presenter = _presenter as? TransactionHistoryRootPresenter else{
                fatalError("declared presenter cannot be parsed for TransactionHistory module")
            }
            
            presenter.view = target.module.activity!.viewController as! TransactionHistoryModuleViewController
            self.set(presenter: presenter)
        }
    }
    
    func show(`from` activity: TransactionHistoryActivities, with transition: Transition, animated: Bool = true){
        self.set(target: .TransactionHistory(activity))
        self.configureModule(activity: activity)
        self.show(with: transition, animated: animated)
    }
}

extension TransactionHistoryWireframe{
    
    func navigate(to activity: TransactionHistoryActivities, with presenter: RootPresenter? = nil){
        var transition: Transition = .present
        
        self.set(presenter: presenter)
        
        switch activity {
        case .TransactionHistoryscreen:
            transition = .root
        case .TransactionDetailscreen:
            transition = .push
        }
        self.show(from: activity, with: transition, animated: true)
    }
    
    func getViewController(from activity: TransactionHistoryActivities)->TransactionHistoryModuleViewController?{
        self.set(target: .TransactionHistory(activity))
        self.configureModule(activity: activity)
        
        guard let module = _target?.module,
            let viewController = module.activity?.viewController as? TransactionHistoryModuleViewController,
            let presenter = self._presenter else { return nil}
        
        viewController.set(presenter: presenter)
        
        return viewController
    }
    
}

extension TransactionHistoryWireframe{
    }
