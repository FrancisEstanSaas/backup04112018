//
//  TransactionDetailViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/1/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class TransactionDetailViewController: TransactionHistoryModuleViewController{
    
    @IBOutlet weak var userAvatar: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        guard let nav = self.navigationController else {
            fatalError("Navigation Controller was not properly set in TransactionDetailViewController")
        }
        
        nav.isNavigationBarHidden = false
    
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        userAvatar.layer.cornerRadius = userAvatar.frame.width / 2
    }
}
