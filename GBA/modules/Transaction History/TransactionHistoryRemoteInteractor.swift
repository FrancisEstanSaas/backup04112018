//
//  TransactionHistoryRemoteInteractor.swift
//  GBA
//
//  Created by Gladys Prado on 26/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

fileprivate enum APICalls: Router{
    
    var baseComponent: String { get { return "public" } }
    
    case getAllTransactionHistories
    case fetchTransactionDetails(transaction_id: Int)
    case fetchReloadDetails(transaction_id: Int)
    
    var route: Route{
        switch self {
        case .getAllTransactionHistories:
            return Route(method: .get,
                         suffix: "/transactions",
                         parameters: nil,
                         waitUntilFinished: true,
                         nonToken: false)
        case .fetchTransactionDetails(let transaction_id):
            return Route(   method              : .get,
                            suffix              : "/transactions/transfers/\(transaction_id)",
                            parameters          : nil,
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .fetchReloadDetails(let transaction_id):
            return Route(   method              : .get,
                            suffix              : "/transactions/reload/\(transaction_id)",
                            parameters          : nil,
                            waitUntilFinished   : true,
                            nonToken            : false)
        }
    }
}

class TransactionHistoryRemoteInteractor: RootRemoteInteractor{
    
    func getAllTransactionHistories(successHandler: @escaping (JSON, ServerReplyCode)->Void){
        NetworkingManager.request(APICalls.getAllTransactionHistories, successHandler: { (reply, replyCode) in
            successHandler(reply, replyCode)
        })
    }
    
    //Transfer Transaction Details
    func FetchTransactionDetails(transaction_id: Int, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.fetchTransactionDetails(transaction_id: transaction_id), successHandler: {
            (reply, statusCode) in
            print(reply,statusCode)
            successHandler(reply, statusCode)
        })
    }
    
    //Reload Transaction Details
    func FetchReloadDetails(transaction_id: Int, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.fetchReloadDetails(transaction_id: transaction_id), successHandler: {
            (reply, statusCode) in
            print(reply,statusCode)
            successHandler(reply, statusCode)
        })
    }
}
