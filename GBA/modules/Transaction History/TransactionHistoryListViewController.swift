//
//  TransactionHistoryListViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/29/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class TransactionHistoryListViewController: TransactionHistoryModuleViewController{
    
    @IBOutlet weak var transactionHistory_tableView: UITableView!
    fileprivate var transactionHistoryHolder:[[String: Any]] = [[String: Any]]()

    private let currentDate = Date.Now
    
    private var currentPresenter: TransactionHistoryListPresenter{
        guard let prsntr = self.presenter as? TransactionHistoryListPresenter else{
            fatalError("Error parsing presenter in ManagePayeeListController")
        }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        transactionHistory_tableView.delegate = self
        transactionHistory_tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Transaction History"
        
        self.navigationController?.isNavigationBarHidden = false
        
        self.reloadTableView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @objc override func backBtn_tapped(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
        
}

extension TransactionHistoryListViewController: UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    /*func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var dateString = ""
        let sectionView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 40)))
        sectionView.backgroundColor = GBAColor.lightGray.rawValue
        
        guard let day = currentDate.day,
            let month = currentDate.monthAbv,
            let year = currentDate.year
            else{ fatalError("Problem in parsing date in dashboard viewController") }
        
        if section == 0{
            dateString = "TODAY • \(day) \(month.uppercased())"
        }else{
            dateString = "YESTERDAY \(day - 1) \(month.uppercased()) \(year)"
        }
        
        let date = UILabel()
            .set(color: GBAColor.darkGray.rawValue)
            .set(alignment: .left)
            .set(fontStyle: GBAText.Font.main(12).rawValue)
            .add(to: sectionView)
            .set(value: dateString)
        
        date.topAnchor.constraint(equalTo: sectionView.topAnchor, constant: 10).Enable()
        date.widthAnchor.constraint(equalTo: sectionView.widthAnchor, multiplier: 9/10).Enable()
        date.centerXAnchor.constraint(equalTo: sectionView.centerXAnchor).Enable()
        date.bottomAnchor.constraint(equalTo: sectionView.bottomAnchor).Enable()
        
        return sectionView

    }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.presenter.wireframe.navigate(to: .TransactionDetailscreen)
    }
    
    func reloadTableView(){
        self.currentPresenter.fetchRemoteTransactionHistoryList(in: 0, successHandler: {
            self.transactionHistoryHolder = self.currentPresenter.transactionHistoryHolder
            self.transactionHistory_tableView.reloadData()
        })
    }
    
}

extension TransactionHistoryListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.transactionHistoryHolder.count > 0 ){
            return self.transactionHistoryHolder.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = Bundle.main.loadNibNamed("TransactionHistoryCellView", owner: self, options: nil)?.first as? TransactionHistoryCellView else{
            fatalError("Parsing TransactionHistoryCellView not properly parsed for TransactionHistoryViewController")
        }
        
        if (self.transactionHistoryHolder.count > 0 ){
            let transactionDetails = self.transactionHistoryHolder[indexPath.row] as NSDictionary
            let transactionAmount = "\(String(describing: transactionDetails["currency"]!)) \(String(describing: transactionDetails["amount"]!))"
            print("transactionDetails ", transactionDetails)
//            let transactionDate = transactionDetails["date"] as? String
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "hh:mm PM"
            
            cell.transactionTitle.text = transactionDetails["name"] as? String
            cell.transactionDate.text = transactionDetails["date"] as? String
            cell.transactionType.text = transactionDetails["trx_type"] as? String
            cell.transactionAmount.text = transactionAmount
        }
        
        return cell
    }
}
