//
//  ReloadOptionTableViewCell.swift
//  GBA
//
//  Created by Gladys Prado on 29/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class ReloadOptionTableViewCell: UITableViewCell {
    
    //MARK: Properties
    @IBOutlet weak var optionImage: UIImageView!
    @IBOutlet weak var optionName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
