//
//  AccountDestinationViewController.swift
//  GBA
//
//  Created by Gladys Prado on 16/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class AccountDestinationViewController: ReloadRootViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblDestinationAccount: UITableView!
    
    //set cell indentifier
    var cellIdentifier = "DestinationAccount"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Proceed",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(tempNextButton))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        
        tblDestinationAccount.delegate = self
        tblDestinationAccount.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "Reload"
        
        self.view.bringSubview(toFront: self.navigationController!.navigationBar)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @objc func tempNextButton() {
        self.presenter.wireframe.navigate(to: .ReloadTransactionSummaryView)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DestinationAccountTableViewCell else {
            fatalError("The dequeued cell is not an instance of ReloadOptionTableViewCell.")
        }
        
        
//        let reloadOptionName = Array(reloadOptions)[indexPath.row].key
//        let reloadOptionImage = Array(reloadOptions)[indexPath.row].value
//
//        cell.optionImage?.image = UIImage(named: reloadOptionImage)
//        cell.optionName?.text = reloadOptionName
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let nav =  self.navigationController else {
            fatalError("")
        }
        
        if (indexPath.row == 0) {
            ReloadWireframe(nav).navigate(to: .ReloadTransactionSummaryView)
        } else {
            ReloadWireframe(nav).navigate(to: .ReloadTransactionSummaryView)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
