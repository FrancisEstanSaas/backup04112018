//
//  ReloadActivities.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

enum ReloadActivities: ViewControllerIdentifier {
    case ReloadOptionsView = "ReloadOptionsView"
    case DirectDebitAccountDepositView = "DirectDebitAccountDepositView"
    case ReloadPhotoUploadView = "ReloadPhotoUploadView"
    case ReloadAmountDepositedView = "ReloadAmountDepositedView"
    case ReloadAccountDestinationView = "ReloadAccountDestinationView"
    case ReloadTransactionSummaryView = "ReloadTransactionSummaryView"
    case CreditCardDepositView = "CreditCardDepositView"
    
    func getPresenter(with viewController: ReloadRootViewController, and wireframe: ReloadWireframe)->RootPresenter?{
        
        switch self {
        case .ReloadOptionsView:
            return ReloadOptionsPresenter(wireframe: wireframe, view: viewController)
        case .DirectDebitAccountDepositView:
            return DirectDebitAccountDepositPresenter(wireframe: wireframe, view: viewController)
        case .ReloadPhotoUploadView:
            return CheckDepositPresenter(wireframe: wireframe, view: viewController)
        case .ReloadAmountDepositedView:
            return CheckDepositPresenter(wireframe: wireframe, view: viewController)
        case .ReloadAccountDestinationView:
            return CheckDepositPresenter(wireframe: wireframe, view: viewController)
        case .ReloadTransactionSummaryView:
            return CheckDepositPresenter(wireframe: wireframe, view: viewController)
        case .CreditCardDepositView:
            return CreditCardDepositPresenter(wireframe: wireframe, view: viewController)
        }
    }
}
