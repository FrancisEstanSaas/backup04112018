//
//  DirectDebitAccountDepositViewController.swift
//  GBA
//
//  Created by Gladys Prado on 16/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class DirectDebitAccountDepositViewController: ReloadRootViewController, GBAFocusableInputViewDelegate {
    
    @IBOutlet weak var txtAccountName: GBATitledTextField!
    @IBOutlet weak var txtABANumber: GBATitledTextField!
    @IBOutlet weak var txtAccountNumber: GBATitledTextField!
    @IBOutlet weak var txtAmount: GBATitledTextField!
    
    @IBOutlet var requiredFields: [GBATitledTextField]!
    
    fileprivate var wallet: Wallet?{
        get{ return self.presenter.interactor.local.wallet }
    }
    
    var directDebitDepositForm: DirectDepositEntity{
        return DirectDepositEntity( wallet_id: "\(self.wallet!.id)",
                                    amount: self.txtAmount.text,
                                    account_name: self.txtAccountName.text,
                                    account_number: self.txtAccountNumber.text,
                                    aba_number: self.txtABANumber.text)
    }
    
    var didEditForm = false
    
    var currentPresenter: DirectDebitAccountDepositPresenter{
        guard let prsntr = self.presenter as? DirectDebitAccountDepositPresenter
            else{ fatalError("Error in parsing presenter for DirectDebitAccountDepositViewController") }
        return prsntr
    }
    
    
    override func viewDidLoad() {
        print("self.presenter ", self.presenter)
        super.viewDidLoad()
        self.presenter.set(view: self)
        
        self.txtAccountName
            .set(self)
            .set(placeholder: "Account Name")
            .set(returnKey: .next)
            .set(next: txtAccountNumber)
            .set(required: true)
            .set(validation: .name(message: "Invalid account name"))
        
        self.txtABANumber
            .set(self)
            .set(placeholder: "ABA Number")
            .set(keyboardType: .numberPad)
            .set(returnKey: .next)
            .set(next: txtAmount)
            .set(required: true)
        
        self.txtAccountNumber
            .set(self)
            .set(placeholder: "Account Number")
            .set(keyboardType: .numberPad)
            .set(returnKey: .next)
            .set(next: txtABANumber)
            .set(required: true)
        
        self.txtAmount
            .set(self)
            .set(placeholder: "Amount")
            .set(keyboardType: .decimalPad)
            .set(returnKey: .done)
            .set(required: true)
        
        self.addBackButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.title = "Reload"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .black
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(submit_tapped(_:)))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        self.view.backgroundColor = GBAColor.white.rawValue
        
    }
    
    override func backBtn_tapped() {
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel your Transaction?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func addBackButton() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(backBtn_tapped))
        
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    @objc private func submit_tapped(_ sender: UIBarButtonItem){
        self.view.subviews.forEach{ $0.resignFirstResponder() }
        /*print(directDebitDepositForm)
        
        if (self.txtAccountName.text != "" && self.txtAccountNumber.text != "" && self.txtABANumber.text != "" && self.txtAmount.text != "") {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.currentPresenter.submitDirectDebitDeposit(directDebitDepositReloadForm: directDebitDepositForm)
        } else {
            self.fieldValidations()
        }*/
        
        var formIsValid = true
        
        self.requiredFields.forEach{
            print($0.isValid())
            if !$0.isValid(){ formIsValid = false; return }
            
        }
        
        if (formIsValid){
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.currentPresenter.submitDirectDebitDeposit(directDebitDepositReloadForm: directDebitDepositForm)
        }
    }
    
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
}

extension DirectDebitAccountDepositViewController: GBATitledTextFieldDelegate{
    func didChange(textField: GBATitledTextField) {
        self.didEditForm = true
    }
}
