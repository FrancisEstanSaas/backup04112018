//
//  DirectDebitAccountDepositPresenter.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

protocol DataReceivedFromDirectDebitDeposit{
    func didReceiveResponse(code: String)
}

class DirectDebitAccountDepositPresenter: ReloadRootPresenter {
    
    var directDebitDepositReloadForm: DirectDepositEntity? = nil
    var dataBridgeForDirectDebitDeposit: DataReceivedFromDirectDebitDeposit? = nil
    
    func submitDirectDebitDeposit(directDebitDepositReloadForm: DirectDepositEntity){
        print(directDebitDepositReloadForm)
        
        self.directDebitDepositReloadForm = directDebitDepositReloadForm
        
        self.interactor.remote.DirectDebitDeposit(form: directDebitDepositReloadForm, successHandler: {
            (reply, statusCode) in
            print("REPLY ",reply)
            print("STATUS CODE ", statusCode)
            
            switch statusCode{
            case .fetchSuccess:
                
                let amount = NSAttributedString(string: "GBP \(String(describing: directDebitDepositReloadForm.amount!))",
                                                attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: GBAText.Size.subContent.rawValue)!])
                let messageContent = NSAttributedString(string: " was successfully deposited into your ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: GBAText.Size.subContent.rawValue)!])
                let destinationAccount = NSAttributedString(string: "Wallet Account",
                                                            attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: GBAText.Size.subContent.rawValue)!])
                
                let content = NSMutableAttributedString()
                content.append(amount)
                content.append(messageContent)
                content.append(destinationAccount)
                
                self.wireframe.presentSuccessPage(title: "Direct Debit Account", message: content, doneAction: {
//                    self.navigationController?.dismiss(animated: true, completion: nil)
                    self.wireframe.popToRootViewController(true)
                })

            case .badRequest:
                print("message not found in server reply: [\(reply)]")
            case .unauthorized:
                guard let message = reply["message"] as? String else{
                    fatalError("Message not found")
                }
                
//                self.showAlert(with: "Notice", message: message, completion: { () } )
                
                let unauthorizedAccessAlert = UIAlertController(title: "Notice", message: message, preferredStyle: .alert)
                
                let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    //dismiss alert
                }
                
                unauthorizedAccessAlert.addAction(confirmAction)
                
                self.view.present(unauthorizedAccessAlert, animated: true, completion: nil)
                self.view.navigationItem.rightBarButtonItem?.isEnabled = true
                
            case .unprocessedData:
                guard let message = reply["message"] as? String else{
                    fatalError("Message not found")
                }
                
//                self.showAlert(with: "Notice", message: message, completion: { () } )
                
                let limitExceededAlert = UIAlertController(title: "Notice", message: message, preferredStyle: .alert)
                
                let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    //dismiss alert
                }
                
                limitExceededAlert.addAction(confirmAction)
                
                self.view.present(limitExceededAlert, animated: true, completion: nil)
                self.view.navigationItem.rightBarButtonItem?.isEnabled = true
            default: break
            }
        })
    }
    
    func cancelConfirmation(message:String){
        let alert = UIAlertController(title: "Warning", message: message, preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.view.navigationController?.dismiss(animated: true, completion: nil)
        }
        
        let no = UIAlertAction(title: "No", style: .cancel) { (_) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(no)
        alert.addAction(yes)
        
        self.view.present(alert, animated: true, completion: nil)
        
    }
}
