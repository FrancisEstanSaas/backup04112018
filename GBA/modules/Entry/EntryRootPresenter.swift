//
//  EntryRootPresenter.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/8/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class EntryRootPresenter: RootPresenter{
    
    var wireframe: EntryWireframe
    var view: EntryModuleViewController
    var interactor: (remote: EntryRemoteInteractor, local: RootLocalInteractor) = (EntryRemoteInteractor(),RootLocalInteractor())
    
    init(wireframe: EntryWireframe, view: EntryModuleViewController){
        self.wireframe = wireframe
        self.view = view
    }
    
    func set(view: EntryModuleViewController){
        self.view = view
    }
    
    func showAlert(with title: String?, message: String, completion: @escaping (()->Void)){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.view.present(alert, animated: true, completion: nil)
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { _ in
            alert.dismiss(animated: true, completion: completion)
        }
    }
}
