//
//  OnboardingData.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/14/17.
//  Copyright © 2017 Republisys. All rights reserved.
//
import UIKit

struct OnboardingData{
    static let contents: [(image: UIImage, title: String, description: String)] =
        [(UIImage(imageLiteralResourceName: "Secured_And_Accessible"), "SECURED & ACCESSIBLE", "Easily access your accounts anytime while keeping your data safe and secured using state of the art encryption"),
         (UIImage(imageLiteralResourceName: "Multiple_Accounts"), "MULTIPLE ACCOUNTS", "Supports multiple accounts with different different currencies that allow conversion real-time"),
         (UIImage(imageLiteralResourceName: "Transfer"), "TRANSFER", "Move money across your own wallets, connect with someone through their wallets and even be able to pay your bills without difficulty"),
         (UIImage(imageLiteralResourceName: "Transaction_History"), "TRANSACTION HISTORY", "We make sure to keep all your transactions from pending history and scheduled updated real-time and accurately")
    ]
}
