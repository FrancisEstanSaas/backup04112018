//
//  Welcome.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 10/23/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import UIKit
import FontAwesome_swift

class Welcome: EntryModuleViewController{
    @IBOutlet var swipeInstruction: [UILabel]!
    @IBOutlet weak var innerView: UIView!
    
    private var firstArrowOppacity = 0.20.Float
    private var initialPanningLocationX: CGFloat? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.layoutArrowInstruction()
        self.innerView.isUserInteractionEnabled = true
        let panToLeft = UIPanGestureRecognizer(target: self, action: #selector(userDid(pan:)))
        self.innerView.addGestureRecognizer(panToLeft)
        
        self.innerView.frame = UIScreen.main.bounds
        self.innerView.apply(gradientLayer: .shadedBlueGreen)
        
        let osVersion = UIDevice.current.systemVersion.components(separatedBy: ".").first!
        
        if osVersion == "11"{
            self.getNextViewForBackground()
        }
    }
    
    func layoutArrowInstruction(){
        for i in 0..<swipeInstruction.count{
            swipeInstruction[i]
                .set(fontStyle: UIFont.fontAwesome(ofSize: swipeInstruction[i].height))
                .set(value: String.fontAwesomeIcon(name: .angleLeft))
                .set(color: UIColor(hexa: 0xFFFFFF))
                .set(alpha: (0.20 * i.Float) + 0.2)
        }
        self.animateArrows()
    }
    
    func animateArrows(){
        UIView.animate(withDuration: 0.2, animations: {
            var alpha = self.firstArrowOppacity
            
            for i in 0..<self.swipeInstruction.count{
                self.swipeInstruction[i].alpha = alpha
                alpha = alpha == 1 ? 0.2.Float : alpha + 0.2
            }
        }, completion:{ _ in
            self.animateArrows()
            self.firstArrowOppacity = self.firstArrowOppacity == 1 ? 0.2: self.firstArrowOppacity + 0.2
        })
    }
    
    @objc private func userDid(pan: UIPanGestureRecognizer){
        let loc = pan.location(in: self.view)
        
        switch pan.state{
        case .began:
            loc.x > (UIScreen.main.bounds.width*0.20) ? (initialPanningLocationX = loc.x) : ()
            print(initialPanningLocationX ?? 0)
            break
        case .changed:
            guard let initLocX = initialPanningLocationX else { return }
            
            if initLocX <= loc.x{ return }
            else{
                self.innerView.frame.origin.x = (UIScreen.main.bounds.width * (loc.x / initLocX)) - UIScreen.main.bounds.width
                self.innerView.layoutIfNeeded()
            }
        case .ended:
            guard let initLocX = initialPanningLocationX else { return }
            
            loc.x / initLocX < 0.5 ?
                (UIView.animate(withDuration: 0.5, animations: {
                    self.innerView.frame.origin.x = -self.innerView.bounds.width
                    
                }, completion: {
                    _ in
                    
                    let VC = self.presenter.wireframe._target?.module.board.instantiateViewController(withIdentifier: EntryActivities.OnBoardingscreen.rawValue)
                    self.navigationController?.setViewControllers([VC!], animated: false)
                })):
                (UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options: [.curveEaseIn, .curveEaseOut], animations: {
                    self.innerView.frame.origin.x = 0
                    
                }, completion: {
                    _ in
                    self.initialPanningLocationX = nil
                    
                }))
        default: break
        }
    }
    
    func getNextViewForBackground(){
        guard let vc = Bundle.main.loadNibNamed("OnboardingViewController", owner: self, options: nil)?.first as? OnboardingViewController,
            let data = OnboardingData.contents.first else { return }
        
        vc.mainImage.image = data.image
        vc.mainContent.text = data.title
        vc.subContent.text = data.description
        
        let nextView = vc.view
        self.view.addSubview(nextView!)
        
        nextView?.translatesAutoresizingMaskIntoConstraints = false
        nextView?.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor).Enable()
        nextView?.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).Enable()
        nextView?.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).Enable()
        nextView?.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).Enable()
        
        self.view.bringSubview(toFront: innerView)
    }
}



















