//
//  ContactSupportViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/20/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class ContactSupportViewController: EntryModuleViewController, UITextViewDelegate {
    
    @IBOutlet weak var name_textField: GBATitledTextField!
    @IBOutlet weak var email_textField: GBATitledTextField!
    @IBOutlet weak var assistCategory_popOver: GBATitledPopOverButton!
    @IBOutlet weak var message_textField: GBATitledTextField!
    @IBOutlet weak var message_area: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    private var selectedConcernType: Concerns = .Dashboard
    private var scrollViewContainer = UIView()
    
    var contactSupportForm: ContactSupportEntity{
        return ContactSupportEntity(customerName: self.name_textField.text, customerEmail: self.email_textField.text, customerType: self.selectedConcernType.type, customerConcern: self.message_area.text)
    }
    
    var currentPresenter: ContactSupportPresenter{
        guard let prsntr = self.presenter as? ContactSupportPresenter
            else{ fatalError("Error in parsing presenter for ContactSupportViewController") }
        return prsntr
    }
    
    override func viewDidLoad() {
        self.addBackButton()
        self.presenter.set(view: self)
        (self._presenter as! ContactSupportPresenter).dataBridge = self


        name_textField
            .set(self)
            .set(alignment: .left)
            .set(placeholder: "Name")
            .set(textColor: GBAColor.black)
            .set(next: email_textField)
        
        email_textField
            .set(self)
            .set(alignment: .left)
            .set(placeholder: "Email Address")
            .set(textColor: GBAColor.black)
            .set(next: assistCategory_popOver)
        
    
        assistCategory_popOver
            .set(title: "How may we assist you?")
            .set(parent: self)
            .set(viewController: GBAPopOverConcernTableview().set(delegate: self))
        
        message_textField
            .set(self)
            .set(alignment: .left)
            .set(placeholder: "Message")
            .set(textColor: GBAColor.black)
        
        let submitBtn = UIBarButtonItem(title: "Submit", style: .plain, target: self, action: #selector(submitBtn_tapped))
        self.navigationItem.rightBarButtonItem = submitBtn
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        //set temporary text area
        message_area.placeholder = "Message"
        message_area.delegate = self
        message_area.layer.borderWidth = 1
        message_area.layer.borderColor = GBAColor.lightGray.rawValue.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Support"
        self.view.backgroundColor = GBAColor.white.rawValue
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @objc func submitBtn_tapped(){
        //&& self.message_textField.text != ""
        if (self.name_textField.text != "" && self.email_textField.text != "" && self.message_area.text != ""){
            self.currentPresenter.processContactSupport(submittedForm: contactSupportForm)
        } else {
            self.testRequiredFields()
        }
    }
    
    /**************************************************************************/
    
    func testRequiredFields(){
        
        self.name_textField
            .set(required: true)
            .set(validation: .name(message: "Invalid customer name"))
        
        self.email_textField
            .set(required: true)
            .set(validation: .email)
    }
    
    /**************************************************************************/
    

    
    func textViewDidBeginEditing(_ textField: UITextView) {
        self.scrollView.setContentOffset(CGPoint.init(x: 0, y: 250), animated: true)
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //text == "\n")
        self.message_area.placeholder = ""
        if (text == "\n") {
            message_area.resignFirstResponder()
            self.testRequiredFields()
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.scrollView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
            return false
        }
        self.testRequiredFields()
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        return true
    }
}
    
    /**************************************************************************/

extension ContactSupportViewController{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension ContactSupportViewController: GBAPopOverDelegate{
    func GBAPopOver(didSelect cellView: UITableViewCell) {
        guard let cell = cellView as? GBAConcernCellView else { fatalError("Error found in parsing cellView to GBACountryCellView") }
        assistCategory_popOver.set(value: cell.concernLabel.text!)
        self.selectedConcernType = cell.concern
        self.message_textField
            .set(text: cell.concernType)
            .set(placeholder: "")
    }
}

extension ContactSupportViewController: DataRecievedContactSupport{
    func didRecievedVerificationData(code: String) {
        self.testRequiredFields()
    }
}

    /**************************************************************************/
    /**************************************************************************/
