//
//  EntryWireframe.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 10/25/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import UIKit


class EntryWireframe: RootWireframe{
    
    
    func configureModule(activity: EntryActivities){
        guard let target =  self._target else{
            fatalError("[EntryWireframe: configureModule] target not found")
        }
        
        if _presenter == nil{
            
            guard let presenter = activity.getPresenter(with: _target?.module.activity?.viewController as! EntryModuleViewController, and: self) as? EntryRootPresenter else{
                fatalError("declared presenter cannot be found in Entry module")
            }
            
            self.set(presenter: presenter)
        }else{
            guard let presenter = _presenter as? EntryRootPresenter else{
                fatalError("declared presenter cannot be parsed for Entry module")
            }
            
            presenter.view = target.module.activity!.viewController as! EntryModuleViewController
            self.set(presenter: presenter)
        }
    }
    
    func show(`from` activity: EntryActivities, with transition: Transition, animated: Bool = true){
        self.set(target: .Entry(activity))
        self.configureModule(activity: activity)
        self.show(with: transition, animated: animated)
    }
}

extension EntryWireframe{
    
    func navigate(to activity: EntryActivities, with presenter: RootPresenter? = nil){
        var transition: Transition = .present
        
        self.set(presenter: presenter)
        
        switch activity {
        case .LaunchScreen:
            transition = .root
        case .Welcomescreen:
            transition = .root
        case .OnBoardingscreen:
            transition = .present
        case .Loginscreen:
            transition = .root
        case .TouchIDLogIn:
            transition = .root
        case .PinCodeLogIn:
            transition = .root
        case .Registrationscreen:
            transition = .root
        case .ForgotPasswordscreen:
            transition = .root
        case .NewPasswordscreen:
            transition = .push
        case .Supportscreen:
            transition = .push
        case .ContactSupportscreen:
            transition = .push
        case .AtmFinderscreen:
            transition = .push
        }
        self.show(from: activity, with: transition, animated: true)
    }
    
    func getViewController(from activity: EntryActivities)->EntryModuleViewController?{
        self.set(target: .Entry(activity))
        self.configureModule(activity: activity)
        
        guard let module = _target?.module,
            let viewController = module.activity?.viewController as? EntryModuleViewController,
            let presenter = self._presenter else { return nil}
        
        viewController.set(presenter: presenter)
        
        return viewController
    }
    
}

extension EntryWireframe{
    
    func presentCodeVerificationViewController(from sender: GBAVerificationCodeDelegate, completion: (()->Void)?, apiCalls: @escaping ((String, GBACodeVerificationViewController)->Void), backAction: @escaping (()->Void)){
        let vc = GBACodeVerificationViewController()
                    .set(delegate: sender)
                    .setApiCall(apiCalls)
                    .setBackButtonAction(backAction)
        
        if completion != nil { vc.setAction(completion!) }
        
        self.navigator.pushViewController(vc, animated: true)
        
    }
    
    func presentSuccessPage(title: String, message: NSAttributedString, doneAction: (()->Void)? = nil){
        let vc = GBASuccessPage()
            .set(title: title)
            .set(message: message)
            .set(doneAction: doneAction)
        
        self.navigator.pushViewController(vc, animated: true)
    }
}









