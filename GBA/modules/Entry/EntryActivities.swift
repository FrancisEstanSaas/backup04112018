//
//  EntryActivities.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 10/23/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//


enum EntryActivities: ViewControllerIdentifier{
    case LaunchScreen = "LaunchScreen"
    case Welcomescreen = "Welcomescreen"
    case OnBoardingscreen = "OnBoardingscreen"
    
    //Login module
    case Loginscreen = "Loginscreen"
    
    //Alternative LogIn modules
    case TouchIDLogIn = "TouchIDLogIn"
    case PinCodeLogIn = "PinCodeLogIn"
    
    //Registration module
    case Registrationscreen = "Registrationscreen"
    
    //Forgot Password module
    case ForgotPasswordscreen = "ForgotPasswordscreen"
    case NewPasswordscreen = "NewPasswordscreen"
    
    //Support module
    case Supportscreen = "Supportscreen"
    case ContactSupportscreen = "ContactSupportscreen"
    
    //AtmFinder module
    case AtmFinderscreen = "AtmFinderscreen"
    
    func getPresenter(with viewController: EntryModuleViewController, and wireframe: EntryWireframe)->RootPresenter?{
        
        switch self {
        case .LaunchScreen:
            return LaunchscreenPresenter(wireframe: wireframe, view: viewController)
        case .Welcomescreen:
            return WelcomePresenter(wireframe: wireframe, view: viewController)
        case .OnBoardingscreen:
            return nil
        case .Loginscreen:
            return LoginPresenter(wireframe: wireframe, view: viewController)
         
        //Alternative LogIn
        case .TouchIDLogIn:
            return TouchIDLogInPresenter(wireframe: wireframe, view: viewController)
        case .PinCodeLogIn:
            return PinCodeLogInPresenter(wireframe: wireframe, view: viewController)
            
        case .Registrationscreen:
            return RegistrationPresenter(wireframe: wireframe, view: viewController)
        case .ForgotPasswordscreen:
            return ForgotPasswordPresenter(wireframe: wireframe, view: viewController)
        case .NewPasswordscreen:
            return nil
        case .Supportscreen:
            return SupportPresenter(wireframe: wireframe, view: viewController)
        case .ContactSupportscreen:
            return ContactSupportPresenter(wireframe: wireframe, view: viewController)
        case .AtmFinderscreen:
            return AtmFinderPresenter(wireframe: wireframe, view: viewController)
        }
        
    }
}

