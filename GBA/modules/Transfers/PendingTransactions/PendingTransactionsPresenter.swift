//
//  PendingTransactionsPresenter.swift
//  GBA
//
//  Created by Gladys Prado on 1/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

protocol DataReceivedFromPendingTransactions{
    func didReceiveResponse(code: String)
}

class PendingTransactionsPresenter: TransfersRootPresenter {
    var pendingTransactionsHolder: [[String: Any]] = [[String: Any]]()
    var completedTransactionsHolder: [[String: Any]] = [[String: Any]]()
    var scheduledTransactionsHolder: [[String: Any]] = [[String: Any]]()
    var transactionDetails: [[String: Any]] = [[String: Any]]()
    var transactionPayeeDetails: [[String: Any]] = [[String: Any]]()
    var transactionID: Int = 0
    
    func fetchPendingTransactions(in page: Int, successHandler: @escaping (()->Void)){
        self.interactor.remote.FetchPendingTransactionsList(successHandler: { (json, statusCode) in
            switch statusCode{
            case .noContent: break
            case .fetchSuccess:
                self.pendingTransactionsHolder.removeAll()
                guard let transactions = json["data"] as? [[String: Any]] else {
                        return
                }
                
                for transaction in transactions {
                    let holder = transaction as [String: Any]
                    self.pendingTransactionsHolder.append(holder)
                }
                
                successHandler()
                
            default: break
            }
        })
    }
    
    func fetchCompletedTransactions(in page: Int, successHandler: @escaping (()->Void)){
        self.interactor.remote.FetchCompletedTransactionsList(successHandler: { (json, statusCode) in
            switch statusCode{
            case .noContent: break
            case .fetchSuccess:
                self.completedTransactionsHolder.removeAll()
                guard let transactions = json["data"] as? [[String: Any]] else {
                        return
                }
                
                for transaction in transactions {
                    let holder = transaction as [String: Any]
                    self.completedTransactionsHolder.append(holder)
                }
                
                successHandler()
                
            default: break
            }
        })
    }
    
    func fetchScheduledTransactions(in page: Int, successHandler: @escaping (()->Void)){
        self.interactor.remote.FetchScheduledTransactionsList(successHandler: { (json, statusCode) in
            switch statusCode{
            case .noContent: break
            case .fetchSuccess:
                self.scheduledTransactionsHolder.removeAll()
                guard let transactions = json["data"] as? [[String: Any]] else {
                        return
                }
                
                for transaction in transactions {
                    let holder = transaction as [String: Any]
                    self.scheduledTransactionsHolder.append(holder)
                }
                
                successHandler()
                
            default: break
            }
        })
    }
    
    func fetchPendingTransactionDetails(transactionID: Int, successHandler: @escaping (()->Void)){
        self.interactor.remote.FetchTransactionDetails(transaction_id: transactionID, successHandler: { (json, statusCode) in
            switch statusCode{
            case .noContent: break
            case .fetchSuccess:
                self.transactionPayeeDetails.removeAll()
                self.transactionDetails.removeAll()
                guard let transactionsPayee = json["payee"] as? [String: Any] else { return }
                self.transactionPayeeDetails.append(transactionsPayee)
                self.transactionDetails.append(json)
                
                successHandler()
            default: break
            }
        })
    }
    
    func cancelPendingTransaction(transactionID: Int, successHandler: @escaping (()->Void)){
        self.interactor.remote.CancelPendingTransaction(transaction_id: transactionID, successHandler: { (reply, statusCode) in
            switch statusCode{
            case .fetchSuccess:
                successHandler()
            case .badRequest:
                guard let code = reply["message"] as? String else{
                    fatalError("Verification code not found in \(reply)")
                }
                self.showAlert(with: "Warning", message: code, completion: {
                    print(code)
                })
            default: break
            }
        })
    }
}
