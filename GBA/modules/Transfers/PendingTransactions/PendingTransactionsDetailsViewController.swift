//
//  PendingTransactionsDetailsViewController.swift
//  GBA
//
//  Created by Gladys Prado on 15/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PendingTransactionsDetailsViewController: TransfersRootViewController {
    
    @IBOutlet weak var btnCancelTransaction: UIButton!
    
    fileprivate var transactionDetailsHolder:[[String: Any]] = [[String: Any]]()
    fileprivate var transactionPayeeDetailsHolder:[[String: Any]] = [[String: Any]]()
    
    //transaction Details
    @IBOutlet weak var lblRecipientName: UILabel!
    @IBOutlet weak var lblRecipientMobileNumber: UILabel!
    @IBOutlet weak var lblTransactionDate: UILabel!
    @IBOutlet weak var lblTransactionNumber: UILabel!
    @IBOutlet weak var lblReferenceNumber: UILabel!
    @IBOutlet weak var lblTransferType: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblSourceAccount: UILabel!
    @IBOutlet weak var lblServiceCharge: UILabel!
    
    private var currentPresenter: PendingTransactionsPresenter{
        guard let prsntr = self.presenter as? PendingTransactionsPresenter else{
            fatalError("Error parsing presenter in ManagePayeeListController")
        }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addBackButton()
        self.btnCancelTransaction.layer.cornerRadius = 5
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "Pending Transactions"
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.displayTransactionDetails()
    }
    
    func displayTransactionDetails(){
        self.transactionDetailsHolder = self.currentPresenter.transactionDetails
        self.transactionPayeeDetailsHolder = self.currentPresenter.transactionPayeeDetails
        print("DISPLAY TRANSACTION DETAILS ", transactionDetailsHolder)
        let transactionDetails = self.transactionDetailsHolder[0] as NSDictionary
        let transactionPayeeDetails = self.transactionPayeeDetailsHolder[0] as NSDictionary
        
        //variable for labels
        let referenceNumber = "\(String(describing: transactionDetails["reference"]!))"
        let transactionNumber = transactionDetails["trx_no"]! as! NSNumber
        let transactionType = transactionDetails["trx_type"]! as! String
        print("transactionNumber ", transactionNumber)
        print("transactionType ", transactionType)
        print("referenceNumber ", referenceNumber)
        
        self.lblRecipientName.text = "\(String(describing: transactionPayeeDetails["name"]!))"
        self.lblRecipientMobileNumber.text = "\(String(describing: transactionPayeeDetails["contact"]!))"
        self.lblTransactionDate.text = "\(String(describing: transactionDetails["date"]!))"
        self.lblTransferType.text = transactionType
        self.lblAmount.text = "\(transactionDetails["currency"]!) \(transactionDetails["amount"]!)"
        self.lblServiceCharge.text = "\(transactionDetails["charges"]!)"
        self.lblSourceAccount.text = "\(transactionDetails["currency"]!) Wallet"
        
        if (transactionNumber == 0){
            self.btnCancelTransaction.isHidden = false
            self.lblTransactionNumber.text = "-------------"
            self.lblReferenceNumber.text = "-------------"
        } else {
            self.btnCancelTransaction.isHidden = true
            self.lblTransactionNumber.text = "\(transactionNumber)"
            self.lblReferenceNumber.text = referenceNumber
        }
    }
    
    @IBAction func didTapCancelTransaction(_ sender: Any) {
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel your transaction?", preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            let transactionDetails = self.transactionDetailsHolder[0] as NSDictionary
            let transactionID = transactionDetails["id"] as? Int
            self.currentPresenter.cancelPendingTransaction(transactionID: transactionID!, successHandler: {
                self.navigationController?.popToRootViewController(animated: true)
            })
        }

        let no = UIAlertAction(title: "No", style: .default, handler: nil)

        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil)
    }
    
}
