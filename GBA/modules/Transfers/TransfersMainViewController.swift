//
//  TransfersMainViewController.swift
//  GBA
//
//  Created by Gladys Prado on 22/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class TransfersMainViewController: TransfersRootViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblTransferOptions: UITableView!
    @IBOutlet weak var btnSeeAllPendingTransactions: UIButton!
    
    //set cell indentifier
    var cellIdentifier = "TransferType"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblTransferOptions.delegate = self
        tblTransferOptions.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Transfers"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //start table view configuration
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ReloadOptionTableViewCell else {
            fatalError("The dequeued cell is not an instance of ReloadOptionTableViewCell.")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.wireframe.navigate(to: .PaySomeoneRecipientAccountView)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    @IBAction func seeAllPendingTransactions(_ sender: Any) {
        let _nav = UINavigationController()
        
        _nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        _nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        _nav.navigationBar.tintColor = GBAColor.white.rawValue
        _nav.isNavigationBarHidden = false
        
        TransfersWireframe(_nav).navigate(to: .PendingTransactionsView)
        self.present(_nav, animated: true, completion: nil)
    }
    
    
}
