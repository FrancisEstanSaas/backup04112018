//
//  PaySomeoneRecipentViewController.swift
//  GBA
//
//  Created by Gladys Prado on 30/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PaySomeoneRecipientViewController: TransfersRootViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblRecipientAccount: UITableView!
    @IBOutlet weak var btnAddNewRecipient: UIButton!
    fileprivate var payeeList: [Payee] = [Payee]()
    
    //set cell indentifier
    var cellIdentifier = "RecipientAccount"
    
    private var currentPresenter: PaySomeonePresenter{
        guard let prsntr = self.presenter as? PaySomeonePresenter else{
            fatalError("Error parsing presenter in ManagePayeeListController")
        }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblRecipientAccount.delegate = self
        tblRecipientAccount.dataSource = self
        
        self.addBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.reloadTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Pay Someone"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func backBtn_tapped() {
//        if currentPresenter.didEditForm!{ self.currentPresenter.cancelConfirmation(message: "This transaction will not be saved.") }
//        else{ self.presenter.wireframe.popFromViewController(true) }
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel your Transaction?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.presenter.wireframe.popFromViewController(true)
        }
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil)
        
//        self.presenter.wireframe.popFromViewController(true)
    }
    
    override func addBackButton() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(backBtn_tapped))
        
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.payeeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let payee = payeeList[indexPath.row]
        
        guard let cell = Bundle.main.loadNibNamed("PayeesCellView", owner: self, options: nil)?.first as? PayeesCellView else {
            fatalError("Error in gathering cellview in nib files")
        }
        
        return cell.set(payee: payee)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentPresenter.payee = payeeList[indexPath.row]
        currentPresenter.didEditForm = true
        
        self.presenter.wireframe.navigate(to: .PaySomeoneSourceAccountView, with: self.currentPresenter)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 49
    }
    
}

extension PaySomeoneRecipientViewController{
    fileprivate func reloadTableView(){
        self.currentPresenter.fetchRemotePayeeList(in: 0, successHandler: {
            self.payeeList = self.currentPresenter.fetchLocalPayeeList() ?? [Payee]()
            self.tblRecipientAccount.reloadData()
        })
    }
}
