//
//  PaySomeoneSourceAccountViewController.swift
//  GBA
//
//  Created by Gladys Prado on 30/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PaySomeoneSourceAccountViewController: TransfersRootViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblSourceAccount: UITableView!
    
    fileprivate var walletList: [Wallet] = [Wallet]()
    
    //set cell indentifier
    var cellIdentifier = "SourceAccount"
    
    //fetch recipient ID
    var recipientID: Int = 0
    
    private var currentPresenter: PaySomeonePresenter{
        guard let prsntr = self.presenter as? PaySomeonePresenter else{
            fatalError("Error parsing presenter in ManagePayeeListController")
        }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblSourceAccount.delegate = self
        tblSourceAccount.dataSource = self
        
        self.addBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.reloadTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Pay Someone"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.title = " "
    }
    
    override func backBtn_tapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func addBackButton() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .normal)
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .highlighted)
        
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    @objc func tempNextButton() {
        guard let nav =  self.navigationController else {
            fatalError("")
        }
        
        TransfersWireframe(nav).navigate(to: .PaySomeoneAmountView)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.walletList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let wallet = walletList[indexPath.row]
        
        guard let cell = Bundle.main.loadNibNamed("DestinationAccountTableViewCell", owner: self, options: nil)?.first as? DestinationAccountTableViewCell else {
            fatalError("Error in parsing cellview in nib files")
        }
        
        return cell.set(wallet: wallet)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let wallet = walletList[indexPath.row]
        currentPresenter.sourceAccount = wallet
        currentPresenter.didEditForm = true
        
        self.presenter.wireframe.navigate(to: .PaySomeoneAmountView, with: self.currentPresenter)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

extension PaySomeoneSourceAccountViewController{
    fileprivate func reloadTableView(){
        self.walletList = self.currentPresenter.fetchLocalWalletList() ?? [Wallet]()
        self.tblSourceAccount.reloadData()
    }
}
