//
//  PaySomeoneTransactionSummaryViewController.swift
//  GBA
//
//  Created by Gladys Prado on 30/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PaySomeoneTransactionSummaryViewController: TransfersRootViewController {
    @IBOutlet weak var lblTransferType: UILabel!
    @IBOutlet weak var lblTransferAmount: UILabel!
    @IBOutlet weak var lblTransferSourceAccount: UILabel!
    @IBOutlet weak var lblCurrentWalletBalance: UILabel!
    @IBOutlet weak var lblServiceCharge: UILabel!
    @IBOutlet weak var lblTotalTransferAmount: UILabel!
    @IBOutlet weak var lblRecipientName: UILabel!
    @IBOutlet weak var lblRecipientNumber: UILabel!
    @IBOutlet weak var lblTransferDate: UILabel!
    
    private var currentPresenter: PaySomeonePresenter{
        guard let prsntr = self.presenter as? PaySomeonePresenter else{
            fatalError("Error parsing presenter in ManagePayeeListController")
        }
        return prsntr
    }
    
    var payToSomeoneEntity: TransferFormEntity{
        return TransferFormEntity( recipient_id: currentPresenter.payee!.uid,
                                   amount: currentPresenter.transferAmount,
                                   date: currentPresenter.transferDate!
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.parent?.title = "Transaction Summary"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(tempSubmitButton))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        
        self.addBackButton()
        self.displayTransactionSummary()
    }
    
    override func backBtn_tapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func addBackButton() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .normal)
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .highlighted)
        
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    func displayTransactionSummary(){
        let displayName = currentPresenter.payee!.nickname == "" ? currentPresenter.payee!.fullname: currentPresenter.payee!.nickname
        let roundedAmountValue = String(format: "%.2f", currentPresenter.transferAmount)
        let roundedWalletBalance = String(format: "%.2f", currentPresenter.sourceAccount!.balance)
        let roundedTotalAmountValue = String(format: "%.2f", currentPresenter.transferAmount)
        self.lblRecipientName.text = displayName
        self.lblRecipientNumber.text = currentPresenter.payee!.mobile
        self.lblTransferDate.text = currentPresenter.transferDate
        self.lblTransferType.text = "Pay Someone"
        self.lblTransferAmount.text = "\(String(describing: currentPresenter.sourceAccount!.currency)) \(roundedAmountValue)"
        self.lblTransferSourceAccount.text = "\(String(describing: currentPresenter.sourceAccount!.currency)) Wallet"
        self.lblCurrentWalletBalance.text = "\(String(describing: currentPresenter.sourceAccount!.currency)) \(roundedWalletBalance)"
        self.lblServiceCharge.text = "\(String(describing: currentPresenter.sourceAccount!.currency)) 0.00"
        self.lblTotalTransferAmount.text = "\(String(describing: currentPresenter.sourceAccount!.currency)) \(roundedTotalAmountValue)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @objc func tempSubmitButton(){
        self.currentPresenter.submitPaySomeoneTransfer(payToSomeoneEntity: payToSomeoneEntity)
    }
    
}
