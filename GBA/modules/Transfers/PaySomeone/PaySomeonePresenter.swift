//
//  PaySomeonePresenter.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

protocol DataReceivedFromPaySomeone{
    func didReceiveResponse(code: String)
}

class PaySomeonePresenter: TransfersRootPresenter {
    
    var payToSomeoneEntity: TransferFormEntity? = nil
    var datataBridgeForPaySomeone: DataReceivedFromPaySomeone? = nil
    var didEditForm: Bool? = false
    
    //data holder for all steps
    var payee: Payee? = nil
    var sourceAccount: Wallet? = nil
    var transferAmount: Double = 0.00
    var transferDate: String? = nil

    func submitPaySomeoneTransfer(payToSomeoneEntity: TransferFormEntity){
        
        self.payToSomeoneEntity = payToSomeoneEntity
        
        self.interactor.remote.TransferPayToSomeone(form: payToSomeoneEntity, successHandler: {
            (reply, statusCode) in
            
            switch statusCode{
            case .fetchSuccess:
                
                let displayName = self.payee!.nickname == "" ? self.payee!.fullname: self.payee!.nickname
                let amount = NSAttributedString(string: "GBP \(String(describing: payToSomeoneEntity.amount))",
                    attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: GBAText.Size.subContent.rawValue)!])
                let messageContent = NSAttributedString(string: " was successfully transferred to ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: GBAText.Size.subContent.rawValue)!])
                let destinationAccount = NSAttributedString(string: displayName!,
                                                            attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: GBAText.Size.subContent.rawValue)!])
                
                let content = NSMutableAttributedString()
                content.append(amount)
                content.append(messageContent)
                content.append(destinationAccount)
                
                self.wireframe.presentSuccessPage(title: "Pay Someone", message: content, doneAction: {
                    self.wireframe.popToRootViewController(true)
                })
                
            case .badRequest:
                print("message not found in server reply: [\(reply)]")
            case .unauthorized:
                guard let message = reply["message"] as? String else{
                    fatalError("Message not found")
                }
                
                let unauthorizedAccessAlert = UIAlertController(title: "Notice", message: message, preferredStyle: .alert)
                
                let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    //dismiss alert
                }
                
                unauthorizedAccessAlert.addAction(confirmAction)
                
                self.view.present(unauthorizedAccessAlert, animated: true, completion: nil)
                self.view.navigationItem.rightBarButtonItem?.isEnabled = true
                
            case .unprocessedData:
                guard let message = reply["message"] as? String else{
                    fatalError("Message not found")
                }
                
                let limitExceededAlert = UIAlertController(title: "Notice", message: message, preferredStyle: .alert)
                
                let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    //dismiss alert
                }
                
                limitExceededAlert.addAction(confirmAction)
                
                self.view.present(limitExceededAlert, animated: true, completion: nil)
                self.view.navigationItem.rightBarButtonItem?.isEnabled = true
            default: break
            }
        })
    }
    
    func fetchLocalPayeeList()->[Payee]?{
        return self.interactor.local.PayeeList
    }
    
    func fetchLocalWalletList()->[Wallet]?{
        return self.interactor.local.wallet
    }
    
    func fetchRemotePayeeList(in page: Int, successHandler: @escaping (()->Void)){
        var payeeList: [Payee]? = [Payee]()
        
        self.interactor.remote.FetchPayeeList(successHandler: { (json, statusCode) in
            switch statusCode{
            case .noContent: break
            case .fetchSuccess:
                guard let rowCount = json["rows"] as? Int,
                    let payees = json["data"] as? [[String: Any]] else{
                        return
                }
                print(rowCount)
                
                payees.forEach{ payeeList?.append(Payee(json: $0)) }
                
                payeeList?.forEach{ $0.append() }
                
                successHandler()
            default: break
            }
        })
    }
    
    func cancelConfirmation(message:String){
        let alert = UIAlertController(title: "Cancel Transaction?", message: message, preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.wireframe.popToRootViewController(true)
        }
        
        let no = UIAlertAction(title: "No", style: .cancel) { (_) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(no)
        alert.addAction(yes)
        
        self.view.present(alert, animated: true, completion: nil)
    }
    
}
