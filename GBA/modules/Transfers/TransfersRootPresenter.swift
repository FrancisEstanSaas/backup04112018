//
//  TransfersRootPresenter.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class TransfersRootPresenter: RootPresenter{
    var wireframe: TransfersWireframe
    
    var interactor: (local: TransfersLocalInteractor, remote: TransfersRemoteInteractor) = (TransfersLocalInteractor(), TransfersRemoteInteractor())
    var view: TransfersRootViewController
    
    init(wireframe: TransfersWireframe, view: TransfersRootViewController){
        self.wireframe = wireframe
        self.view = view
    }
    
    func set(view: TransfersRootViewController){
        self.view = view
    }
    
    func showAlert(with title: String?, message: String, completion: @escaping (()->Void)){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.view.present(alert, animated: true, completion: nil)
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { _ in
            alert.dismiss(animated: true, completion: completion)
        }
    }
}
