//
//  TransfersActivities.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

enum TransfersActivities: ViewControllerIdentifier {
    case TransfersMainView = "TransfersMainView"
    case PayBillsMainView = "PayBillsMainView"
    case PaySomeoneRecipientAccountView = "PaySomeoneRecipientAccountView"
    case PaySomeoneSourceAccountView = "PaySomeoneSourceAccountView"
    case PaySomeoneAmountView = "PaySomeoneAmountView"
    case PaySomeoneTransferScheduleView = "PaySomeoneTransferScheduleView"
    case PaySomeoneTransactionSummaryView = "PaySomeoneTransactionSummaryView"
    case PendingTransactionsView = "PendingTransactionsView"
    case PendingTransactionsDetailsView = "PendingTransactionsDetailsView"
    
    
    func getPresenter(with viewController: TransfersRootViewController, and wireframe: TransfersWireframe)->RootPresenter?{
        
        switch self {
        case .TransfersMainView:
            return TransferOptionsPresenter(wireframe: wireframe, view: viewController)
        case .PayBillsMainView:
            return BillsPaymentPresenter(wireframe: wireframe, view: viewController)
        case .PaySomeoneRecipientAccountView:
            return PaySomeonePresenter(wireframe: wireframe, view: viewController)
        case .PaySomeoneSourceAccountView:
            return PaySomeonePresenter(wireframe: wireframe, view: viewController)
        case .PaySomeoneAmountView:
            return PaySomeonePresenter(wireframe: wireframe, view: viewController)
        case .PaySomeoneTransferScheduleView:
            return PaySomeonePresenter(wireframe: wireframe, view: viewController)
        case .PaySomeoneTransactionSummaryView:
            return PaySomeonePresenter(wireframe: wireframe, view: viewController)
        case .PendingTransactionsView:
            return PendingTransactionsPresenter(wireframe: wireframe, view: viewController)
        case .PendingTransactionsDetailsView:
            return PendingTransactionsPresenter(wireframe: wireframe, view: viewController)
        }
    }
}
