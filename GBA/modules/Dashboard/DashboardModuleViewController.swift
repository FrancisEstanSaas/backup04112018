//
//  DashboardModuleViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/20/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class DashboardModuleViewController: RootViewController{
    
    var presenter: DashboardRootPresenter{
        get{
            let prsntr = self._presenter as! DashboardRootPresenter
            return prsntr
        }
    }
    
}
