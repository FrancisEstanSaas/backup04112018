//
//  DashboardViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/20/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift
import CryptoSwift


class DashboardViewController: DashboardModuleViewController{
    @IBOutlet weak var reloadControl_container: UIView!
    @IBOutlet weak var reload_btn: UIButton!
    @IBOutlet weak var clickableArea: UIView!
    @IBOutlet weak var balance_label: UILabel!
    
    @IBOutlet weak var cardAspectRatio: NSLayoutConstraint!
    @IBOutlet weak var cardImageView_contrainer: GBACard!
    @IBOutlet weak var pullCardDrawer_btn: UIButton!
    
    @IBOutlet weak var transactionHistory_tableView: UITableView!
    
    @IBOutlet weak var viewEmptyStateHolder: UIView!
    
    //API Connection
    fileprivate var transactionHistoryHolder:[[String: Any]] = [[String: Any]]()
    
    private var cardIsShown: Bool = true
    
    private var currentDate = Date.Now

    fileprivate var wallet: Wallet?{
        get{ return self.presenter.interactor.local.wallet }
    }
    
    fileprivate var profile: User?{
        get{ return self.presenter.interactor.local.userProfile }
    }
    
    private var currentPresenter: DashboardRootPresenter{
        guard let prsntr = self.presenter as? DashboardRootPresenter else{
            fatalError("Error parsing presenter in DashboardRootPresenter")
        }
        return prsntr
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.bringSubview(toFront: self.navigationController!.navigationBar)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let notifIcon = UIBarButtonItem(image: UIImage(ciImage: CIImage(image: UIImage(named: "Notification")!)!,
                                                       scale: 12,
                                                       orientation: .up),
                                        style: .plain,
                                        target: self,
                                        action: #selector(navigateToNotifications(_:)))
        
        
        self.parent?.navigationItem.rightBarButtonItem = notifIcon
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        
        self.reload_btn.layer.borderWidth = 1
        self.reload_btn.layer.borderColor = GBAColor.white.rawValue.cgColor
        self.reload_btn.layer.cornerRadius = 3
        
        self.balance_label.font = GBAText.Font.main(GBAText.Size.subContent.rawValue).rawValue
        
        self.pullCardDrawer_btn.titleLabel?.font = UIFont.fontAwesome(ofSize: 17)
        self.pullCardDrawer_btn.setTitle(String.fontAwesomeIcon(name: .angleUp), for: .normal)
        
        self.transactionHistory_tableView.delegate = self
        self.transactionHistory_tableView.dataSource = self
        
        self.transactionHistory_tableView.backgroundColor = GBAColor.lightGray.rawValue
        
        let reloadContainer_tapped = UITapGestureRecognizer(target: self, action: #selector(toggleCardDrawer_tapped(_:)))
        
        self.clickableArea.isUserInteractionEnabled = true
        self.clickableArea.addGestureRecognizer(reloadContainer_tapped)
        
        self.reloadTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Dashboard"
        
        self.presenter.fetchUserInfo {
            self.encryptUserInfo()
            self.presenter.fetchWallet {
                self.repopulateCardInfo()
            }
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.parent?.title = " "
        self.parent?.navigationItem.rightBarButtonItem = nil
    }
    
    @IBAction func toggleCardDrawer_tapped(_ sender: Any) {
        guard let ratio = self.cardAspectRatio else { return }
        self.cardAspectRatio.Disable()
        
        switch self.cardIsShown {
        case true:
            UIView.animate(withDuration: 1) {
                self.cardAspectRatio = ratio.firstItem?.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.01).Enable()
                self.cardImageView_contrainer.alpha = 0
                self.pullCardDrawer_btn.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                self.view.layoutIfNeeded()
            }
        case false:
            self.cardImageView_contrainer.set(isAnimating: true)
            UIView.animate(withDuration: 0.5, animations: {
                self.pullCardDrawer_btn.transform = CGAffineTransform(rotationAngle: 0)
                self.cardAspectRatio = ratio.firstItem!.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 10/45).Enable()
                self.cardImageView_contrainer.alpha = 1
                self.view.layoutIfNeeded()
            }, completion: { _ in
                UIView.animate(withDuration: 0.5){
                    self.cardImageView_contrainer.set(isAnimating: false)
                    self.view.layoutIfNeeded()
                }
            })
            
        }
        
        self.cardIsShown = !self.cardIsShown
    }
    @IBAction func reloadBtn_tapped(_ sender: UIButton) {
        let _nav = UINavigationController()
        
        _nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        _nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        _nav.navigationBar.tintColor = GBAColor.white.rawValue
        _nav.isNavigationBarHidden = false
        
        ReloadWireframe(_nav).navigate(to: .ReloadOptionsView)
        self.present(_nav, animated: true, completion: nil)
    }
    
    private func repopulateCardInfo(){
        guard let userProfile = self.profile,
            let userWallet = self.wallet else{
            return
        }
        
        let firstName = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.firstname)
        let lastName = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.lastname)
        
        
        let userFullName = "\(firstName) \(lastName)"
        
        self.cardImageView_contrainer
            .set(owner: userFullName)
        
        self.balance_label.text = "\(userWallet.currency) \(userWallet.balance.toString)"
        self.cardImageView_contrainer.layoutSubviews()
    }
    
    //notifications button
    @objc private func navigateToNotifications(_ sender: UIBarButtonItem){
        let nav = UINavigationController()
        nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        nav.navigationBar.tintColor = GBAColor.white.rawValue
        nav.isNavigationBarHidden = false
        
        NotificationsWireframe(nav).navigate(to: .NotificationsListView)
        
        self.present(nav, animated: true, completion: nil)
    }
    func encryptUserData(key: String, iv: String, userData: String) -> String {
        let data = userData.data(using: .utf8)!
        let encrypted = try! AES(key: key, iv: iv).encrypt([UInt8](data))
        let encryptedData = Data(encrypted)
        return encryptedData.base64EncodedString()
    }
    
    func decryptUserData(key: String, iv: String, userData: String) -> String {
        let data = Data(base64Encoded: userData)!
        let decrypted = try! AES(key: key, iv: iv).decrypt([UInt8](data))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
    }
    
    func encryptUserInfo() {
        let newUser = User()
        let userProfile = self.profile!
        newUser.id = userProfile.id
        newUser.firstname = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.firstname)
        newUser.lastname = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.lastname)
        newUser.mobile = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.mobile)
        newUser.email = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.email)
        
        do {
            try GBARealm.write {
                GBARealm.add(newUser, update: true)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
}

extension DashboardViewController: UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /*func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var dateS tring = ""
        let sectionView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 40)))
        sectionView.backgroundColor = GBAColor.lightGray.rawValue
        
        guard let day = currentDate.day,
            let month = currentDate.monthAbv,
            let year = currentDate.year
            else{ fatalError("Problem in parsing date in dashboard viewController") }
        
        if section == 0{
            dateString = "TODAY \(day) \(month.uppercased()) \(year)"
        }else{
            dateString = "YESTERDAY \(day - 1) \(month.uppercased()) \(year)"
        }
        
        let date = UILabel()
            .set(color: GBAColor.darkGray.rawValue)
            .set(alignment: .left)
            .set(fontStyle: GBAText.Font.main(12).rawValue)
            .add(to: sectionView)
            .set(value: dateString)

        date.topAnchor.constraint(equalTo: sectionView.topAnchor, constant: 10).Enable()
        date.widthAnchor.constraint(equalTo: sectionView.widthAnchor, multiplier: 9/10).Enable()
        date.centerXAnchor.constraint(equalTo: sectionView.centerXAnchor).Enable()
        date.bottomAnchor.constraint(equalTo: sectionView.bottomAnchor).Enable()

        return sectionView
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*let index = indexPath.row
        
        let cells = indexPath.section == 0 ? cells01 : cells02
        
        let cell = cells[index]
        if index == 0{
            cell.set(name: "sample text here boom!")
            cell.position = .top
        }else if index > 0 && index < cells.count - 1{
            cell.position = .middle
        }else if index == cells.count - 1{
            cell.position = .bottom
        }
        
        return cell*/
        
        guard let cell = Bundle.main.loadNibNamed("TransactionHistoryCellView", owner: self, options: nil)?.first as? TransactionHistoryCellView else{
            fatalError("Parsing TransactionHistoryCellView not properly parsed for TransactionHistoryViewController")
        }
        
        if (self.transactionHistoryHolder.count > 0 ){
            let transactionDetails = self.transactionHistoryHolder[indexPath.row] as NSDictionary
            let transactionAmount = "\(String(describing: transactionDetails["currency"]!)) \(String(describing: transactionDetails["amount"]!))"
            print("transactionDetails ", transactionDetails)
//            let transactionDate = transactionDetails["date"] as? String
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "hh:mm PM"
            
            cell.transactionTitle.text = transactionDetails["name"] as? String
            cell.transactionDate.text = transactionDetails["date"] as? String
            cell.transactionType.text = transactionDetails["trx_type"] as? String
            cell.transactionAmount.text = transactionAmount
        }
        
        return cell
        
        
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    func reloadTableView(){
        self.currentPresenter.fetchRemoteTransactionHistoryList(in: 0, successHandler: {
            self.transactionHistoryHolder = self.currentPresenter.transactionHistoryHolder
            self.transactionHistory_tableView.reloadData()
        })
    }
    
}

extension DashboardViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return section == 0 ? cells01.count: cells02.count
//        if (self.transactionHistoryHolder.count > 0 ){
//            return self.transactionHistoryHolder.count
//        } else {
//            return 1
//        }
        
        (self.transactionHistoryHolder.count > 0) ? (self.viewEmptyStateHolder.isHidden = true) : (self.viewEmptyStateHolder.isHidden = false)
        return self.transactionHistoryHolder.count
    }
}



