//
//  SettingsRemoteInteractor.swift
//  GBA
//
//  Created by Gladys Prado on 8/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//
import Foundation
import UIKit
import Alamofire

fileprivate enum APICalls: Router {
    
    var baseComponent: String { get { return "/public" } }
    
    //Cases
    case changeVerification(authLevel: String)
    case editProfileName(firstname: String, lastname: String)
    case updatePassword(oldpassword: String, newpassword: String, newpasswordconfirm: String)
    case uploadProfilePicture(profilePicture: MultipartFormData)
    case uploadProfileImage(profilePicture: Data)
    case setRegisterDevice(token: String, uuid: String, code: String)
    case setDeviceRequest(uuid: String)
    case setChangeDevice(token: String, uuid: String, code: String)
    
    //Router Setup
    var route: Route {
        
        switch self {
        case .changeVerification(let authLevel):
            return Route(method: .patch,
                         suffix: "/verification",
                         parameters: ["auth_level" : authLevel],
                         waitUntilFinished: true,
                         nonToken: true)
            
        case .editProfileName(let firstname, let lastname):
            return Route(method: .post,
                         suffix: "/profile",
                         parameters: ["firstname" : firstname,
                                      "lastname" : lastname],
                         waitUntilFinished: true,
                         nonToken: true)
        case .updatePassword(let oldpassword, let newpassword, let newpasswordconfirm):
            return Route(method: .post,
                         suffix: "/change/password",
                         parameters: ["old_password": oldpassword,
                                      "password": newpassword,
                                      "password_confirm": newpasswordconfirm],
                         waitUntilFinished: true,
                         nonToken: true)
            
//        case .uploadProfilePicture():
//            return Route(method: .post,
//                         suffix: "/images",
//                         parameters: ["is_profile" : "1"],
//                         waitUntilFinished: true,
//                         nonToken: false)
            
        case .uploadProfilePicture(let profilePicture):
            return Route(method: .post,
                         suffix: "/images",
                         parameters: ["photo" : profilePicture,
                                      "is_profile" : "1"],
                         waitUntilFinished: true,
                         nonToken: false)
            
        case .uploadProfileImage(let profilePicture):
            return Route(method: .post,
                         suffix: "/images",
                         parameters: ["photo" : profilePicture,
                                      "is_profile" : "1"],
                         waitUntilFinished: true,
                         nonToken: false)
            
        case .setRegisterDevice(let token, let uuid, let code):
            return Route(method: .post,
                         suffix: "/register-device",
                         parameters: ["token": token,
                                      "uuid": uuid,
                                      "code": code],
                         waitUntilFinished: true,
                         nonToken: false)
            
        case .setDeviceRequest(let uuid):
            return Route(method: .post,
                         suffix: "/device/request",
                         parameters: ["uuid": uuid],
                         waitUntilFinished: true,
                         nonToken: false)
        
        case .setChangeDevice(let token, let uuid, let code):
            return Route(method: .post,
                         suffix: "/device/change",
                         parameters: ["token": token,
                                      "uuid": uuid,
                                      "code": code],
                         waitUntilFinished: true,
                         nonToken: false)
        }
    }
}

class SettingsRemoteInteractor: RootRemoteInteractor{
    
    // Contact Support API Calls
    
    func submitChangeVerificationLvl(form: String, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.changeVerification(authLevel: form),
                                  successHandler: {
                                    (reply, statusCode) in
                                    successHandler(reply, statusCode)
        })
    }
    
    func submitEditProfileInfo(form: EditProfile, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.editProfileName(firstname: form.firstName, lastname: form.lastName),
                                  successHandler: {
                                    (reply, statusCode) in
                                    successHandler(reply, statusCode)
        })
    }
    
    func submitNewPassword(form: UpdatePasswordFormEntity, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.updatePassword(oldpassword: form.oldPassword, newpassword: form.newPassword, newpasswordconfirm: form.newPasswordConfirm),
                                  successHandler: {
                                    (reply, statusCode) in
                                    successHandler(reply, statusCode)
        })
    }
    
    //upload profile picture
//    func uploadProfilePicture(multipartFormData: @escaping (MultipartFormData) -> Void, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
//        NetworkingManager.upload(APICalls.uploadProfilePicture(), multipartFormData: multipartFormData, successHandler: {
//            (reply, statusCode) in
//            successHandler(reply, statusCode)
//        })
//    }
    
    func uploadProfilePicture(form: MultipartFormData, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.uploadProfilePicture(profilePicture: form), successHandler: {
            (reply, statusCode) in
            successHandler(reply, statusCode)
        })
    }

    func uploadProfileImage(form: Data, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.upload(APICalls.uploadProfileImage(profilePicture: form), imageData: form, successHandler: {
            (reply, statusCode) in
            successHandler(reply, statusCode)
        })
    }
    
    func setDeviceRequest(uuid: String, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.setDeviceRequest(uuid: uuid), successHandler: {
            (reply, statusCode) in
            successHandler(reply, statusCode)
            })
    }
        
    
    func setRegisterDevice(token: String, uuid: String, code: String, successHandler: @escaping ((JSON, ServerReplyCode)->Void)) {
        NetworkingManager.request(APICalls.setRegisterDevice(token: token, uuid: uuid, code: code), successHandler: {
            (reply, statusCode) in
            successHandler(reply, statusCode)
        })
    }
    
    func setChangeDevice(token: String, uuid: String, code: String, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.setChangeDevice(token: token, uuid: uuid, code: code), successHandler: {
            (reply, statusCode) in
            successHandler(reply, statusCode)
        })
    }
    
}

