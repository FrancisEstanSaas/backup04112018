//
//  SettingsRootViewController.swift
//  GBA
//
//  Created by Gladys Prado on 6/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

class SettingsRootViewController: RootViewController{
    
    var presenter: SettingsRootPresenter{
        get{
            let prsntr = self._presenter as! SettingsRootPresenter
            return prsntr
        }
    }

    
}
