//
//  MyCardViewController.swift
//  GBA
//
//  Created by Gladys Prado on 6/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit
import CryptoSwift

class MyCardViewController: SettingsRootViewController {
    
    @IBOutlet weak var cardImageView_container: GBACard!
    
    fileprivate var profile: User?{
        get{ return self.presenter.interactor.local.userProfile }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.repopulateCardInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.title = "My Card"
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.title = " "
    }
    
    func decryptUserData(key: String, iv: String, userData: String) -> String {
        let data = Data(base64Encoded: userData)!
        let decrypted = try! AES(key: key, iv: iv).decrypt([UInt8](data))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
    }
        

    private func repopulateCardInfo(){
        guard let userProfile = self.profile else{
                return
        }
        
        let firstName = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.firstname)
        let lastName = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.lastname)
        
        let userFullName = "\(firstName) \(lastName)"
        
        self.cardImageView_container
            .set(owner: userFullName)
        
        self.cardImageView_container.layoutSubviews()
    }
}
