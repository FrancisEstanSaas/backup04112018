//
//  Network.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/7/17.
//  Copyright © 2017 Republisys. All rights reservded.
//

import Foundation
import Alamofire
import RealmSwift
// Base Address and Headers (replace as needed)
private let GBAServerBaseAddress = "http://10.10.10.30:80/gba-webservice-1.0" //local - development
//private let GBAServerBaseAddress = "http://103.225.39.7/gba_webservice-1.0_live" //public - release
//private let GBAServerBaseAddress = "http://test.gbaccess.com" //public - development
//private let GBAServerBaseAddress = "http://dev.gbaccess.com" //public - staging
private var serverHeaders =
    [   "Accept" : "application/json",
        "app_id": "1",
        "app_secret": "vOHCJUM53VImmHp9MynhvV6Jd3Fp24nhm81DfChg",
        "client_id": "1",
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQyMjYxZmFkMTJmMjExZTM1MTkzZWEwNzg1YzhhMjM3NjU1MWQwNGQ1MmUxMjlhYTVhZTlmNzVjYzljZTA3ZmUyNGM3MTRmZjI4YTE0MjY2In0.eyJhdWQiOiIxIiwianRpIjoiZDIyNjFmYWQxMmYyMTFlMzUxOTNlYTA3ODVjOGEyMzc2NTUxZDA0ZDUyZTEyOWFhNWFlOWY3NWNjOWNlMDdmZTI0YzcxNGZmMjhhMTQyNjYiLCJpYXQiOjE1MTYzMzc0ODIsIm5iZiI6MTUxNjMzNzQ4MiwiZXhwIjoxNTQ3ODczNDgyLCJzdWIiOiIyMSIsInNjb3BlcyI6WyJwcmltYXJ5Il19.Zbe9RU9Ts4v0UdQWIeggYvxPtFgYa7GfV9W28YvjC3hEN_loZyg8h3Ti_GYE-nnIqcEMuJ9eMI9E4T_P8SJlZK7g0cYbrbRFirp3PTlUJYOR9AEeTep5KnSIixsdjvGAYP4bjDbtwdIlBlc96pL7pl4qqMAtpfJi4JHwxE8EcUdqy92qexNLpRi-sSNq8LCCmsOoqPLbD60fk9A2bJh07E0KqdKWo0e83v4jwTMSL2Q0ivQs5G3SeYCKU4lePTJAd6iGAEv1fq30bLmxkHBESlbNfppcyBtXUuiFcOCqO8ucTUmE0vJnYU0vg5dEO-Bp0Ssqz-o8iO6vYBFDZN9CVvT9pGLG8Bx994g6mVQurGtSVLJ-UfC7tQLvt442j5eIUJPK-3diXIySA1tddH3dGRYz9dVT0_1eH9TnZo1OaOK5_XftTqjvu-1KfnmcWCmvr17_p6kyDj0lqcS9A6UNG5dDkSwR1OvkBPu6IgHpZktZ5TZSXjLd3XYOHnjqSkh236S9cvuIOhZ87Vx6d1jCHYQk01tnJJeB5ldXCDfMWKCLqG6AXN4RljBOjWjqoMQ3HNnHI3YgLOklZyG8FJbez2lB0Bo60qnVBweN_327w1caHm8d7FovThm6eDesBq0mWgOjyK9Kpq1mrykddEvUHaAy1sBsJFQlomkTccSozKc"]

// MARK: - Router Protocol and Route Struct

enum GBANetworkError:Error{
    case tokenNotFound
}

struct Route{
    var method: HTTPMethod
    var suffix: String
    var token: String? = nil
    var parameters: Parameters?
    var waitUntilFinished: Bool
    var nonToken: Bool
    
    init(method: HTTPMethod, suffix: String, parameters: Parameters? = nil, waitUntilFinished: Bool = true, nonToken: Bool = true){
        self.method = method
        self.suffix = suffix
        parameters != nil ? (self.parameters = parameters!): ()
        self.waitUntilFinished = waitUntilFinished
        self.nonToken = nonToken
    }
}

protocol Router: URLRequestConvertible{
    var baseComponent: String { get }
    
    var route: Route { get }
}

extension Router{
    func asURLRequest() throws -> URLRequest {
        let method: HTTPMethod = self.route.method
        var url: URL = URL(string: GBAServerBaseAddress)!.appendingPathComponent(self.baseComponent)    //local
        
//        var url: URL = URL(string: GBAServerBaseAddress)!                                             //remote
        if self.route.suffix != "" { url = url.appendingPathComponent(self.route.suffix) }
        _ = (self.route.token != nil) ? (serverHeaders.updateValue(route.token!, forKey: "token")): ("")
        
        let parameters: Parameters? = self.route.parameters
        
        if !route.nonToken{
            do{
                let accessToken = try self.fetchToken()
                serverHeaders.updateValue("Bearer \(accessToken)", forKey: "Authorization")
            }catch let error as GBANetworkError{
                switch error{
                case .tokenNotFound:
                    let nav = UINavigationController()
                    EntryWireframe(nav).navigate(to: .Loginscreen)
                    throw error
                }
            }
        }
        
        let urlRequest = try URLRequest(url: url, method: method, headers: serverHeaders)
        
        return try Alamofire.URLEncoding().encode(urlRequest, with: parameters)
    }
    
    private func fetchToken()throws->String{
        let authData = GBARealm.objects(UserAuthentication.self)
        
        guard let latestToken = authData.last?.accessToken else{
            throw GBANetworkError.tokenNotFound
        }
        
        return latestToken
    }
}

enum ServerReplyCode: Int{
    case fetchSuccess           = 200
    case dataCreated            = 201
    case accepted               = 202
    case noContent              = 204
    case notModified            = 304
    case badRequest             = 400
    case unauthorized           = 401
    case forbidden              = 403
    case notFound               = 404
    case conflict               = 409
    case unprocessedData        = 422
    case internalServerError    = 500
    case serviceUnavailable     = 503
}

fileprivate var errorAction: (Error)->() = {
    error in
    print("Network Error: \(error.localizedDescription)")
}

class NetworkingManager{
    
    private(set)var Token: (String, String) = (access: "", "")
    
    class func request(_ router: Router,
                       successHandler: @escaping (JSON, ServerReplyCode) -> Void,
                       errorHandler: @escaping (Error)->Void = errorAction){
        print(serverHeaders)
        do{
            
            var urlRequest = try router.asURLRequest()
            
            if let body = urlRequest.httpBody{
                let bodyString = String(data: body, encoding: String.Encoding.utf8)!
                var bodyDict = [String: String]()
                
                for keyValueString in bodyString.components(separatedBy: "&"){
                    var parts = keyValueString.components(separatedBy: "=")
                    
                    if parts.count < 2 { continue }
                    
                    let key = parts[0].removingPercentEncoding!
                    let value = parts[1].removingPercentEncoding!
                    
                    bodyDict[key] = value
                }
            }
            
            var loadingIndicator: LoadingIndicatorView?
            
            if router.route.waitUntilFinished {
                loadingIndicator = LoadingIndicatorView()
                UIApplication.shared.keyWindow!.addSubview(loadingIndicator!)
            }
            
            Alamofire.request(router).responseJSON{
                data in
                
                switch data.result{
                case .success(let json):
                    guard let statusCode = data.response?.statusCode else { fatalError("Status code not found in reply") }
                    print("Request Success from \(String(describing: urlRequest.url?.absoluteString))")
                    successHandler(json as? JSON ?? ["value": "nil"], ServerReplyCode(rawValue: statusCode)!)
                case .failure(let error):
                    print("\n\n===========Error===========")
                    print("Error Code: \(error._code)")
                    print("Error Messsage: \(error.localizedDescription)")
                    if let data = data.data, let str = String(data: data, encoding: String.Encoding.utf8){
                        print("Server Error: " + str)
                    }
                    debugPrint(error as Any)
                    print("===========================\n\n")
                }
                
                if let loadingIndicator = loadingIndicator {
                    loadingIndicator.removeFromSuperview()
                }
            }
            
        }catch{
            errorHandler(error)
        }
        
    }
    
    var multipartFormData = MultipartFormData()
    
    //test function
    class func upload(_ router: Router, imageData: Data,
                      successHandler: @escaping (JSON, ServerReplyCode) -> Void,
                      errorHandler: @escaping (Error)->Void = errorAction){
        print(serverHeaders)
        do{
            
            var urlRequest = try router.asURLRequest()
            
            if let body = urlRequest.httpBody{
                let bodyString = String(data: body, encoding: String.Encoding.utf8)!
                var bodyDict = [String: String]()
                
                for keyValueString in bodyString.components(separatedBy: "&"){
                    var parts = keyValueString.components(separatedBy: "=")
                    
                    if parts.count < 2 { continue }
                    
                    let key = parts[0].removingPercentEncoding!
                    let value = parts[1].removingPercentEncoding!
                    
                    bodyDict[key] = value
                }
            }
            
            var loadingIndicator: LoadingIndicatorView?
            
            if router.route.waitUntilFinished {
                loadingIndicator = LoadingIndicatorView()
                UIApplication.shared.keyWindow!.addSubview(loadingIndicator!)
            }
            
            Alamofire.request(router).responseJSON{
                data in
                
                switch data.result{
                case .success(let json):
                    guard let statusCode = data.response?.statusCode else { fatalError("Status code not found in reply") }
                    print("Request Success from \(String(describing: urlRequest.url?.absoluteString))")
                    successHandler(json as? JSON ?? ["value": "nil"], ServerReplyCode(rawValue: statusCode)!)
                case .failure(let error):
                    print("\n\n===========Error===========")
                    print("Error Code: \(error._code)")
                    print("Error Messsage: \(error.localizedDescription)")
                    if let data = data.data, let str = String(data: data, encoding: String.Encoding.utf8){
                        print("Server Error: " + str)
                    }
                    debugPrint(error as Any)
                    print("===========================\n\n")
                }
                
                if let loadingIndicator = loadingIndicator {
                    loadingIndicator.removeFromSuperview()
                }
            }
            
        }catch{
            errorHandler(error)
        }
        
    }
    
    
}


//
//        Alamofire.upload(multipartFormData: { multipartFormData in
//            multipartFormData.append(imageData,
//                                     withName: "imagefile",
//                                     fileName: "image.jpg",
//                                     mimeType: "image/jpeg")
//        },
//                         to: "http://10.10.10.30:80/gba-webservice-1.0/images",
//                         headers: serverHeaders,
//                         encodingCompletion: { encodingResult in
//                            switch encodingResult {
//                            case .success(let upload, _, _):
//                                upload.uploadProgress(closure: { (Progress) in
//                                    print("Upload Progress: \(Progress.fractionCompleted)")
//                                })
//                                upload.validate()
//                                upload.responseJSON { response in
//                                    debugPrint("SUCCESS ", response)
//
//                                }
//                            case .failure(let encodingError):
//                                print(encodingError)
//                            }
//        })








