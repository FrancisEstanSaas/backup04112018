



import UIKit

extension UILabel{
    
    @discardableResult
    func set(value: String)->Self{
        self.text = value
        return self
    }
    @discardableResult
    func set(lines: Int)->Self{
        self.numberOfLines = lines
        return self
    }
    
    @discardableResult
    func set(size: CGFloat)->Self{
        self.font = GBAText.Font.main(size).rawValue
        return self
    }
    
    @discardableResult
    func set(color: UIColor)->Self{
        self.textColor = color
        return self
    }
    
    @discardableResult
    func set(fontStyle: UIFont)->Self{
        self.font = fontStyle
        return self
    }
    
    @discardableResult
    func set(alignment: NSTextAlignment)->Self{
        self.textAlignment = alignment
        return self
    }
    
    @discardableResult
    func set(alpha: CGFloat)->Self{
        self.alpha = alpha
        return self
    }
    
    @discardableResult
    func add(to view: UIView)->Self{
        view.addSubview(self)
        self.translatesAutoresizingMaskIntoConstraints = false
        return self
    }
    
    
}
