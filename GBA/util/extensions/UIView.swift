



import UIKit.UIView

extension UIView{
    var height  : CGFloat { get { return self.bounds.height } }
    var width   : CGFloat { get { return self.bounds.width } }
    
    func apply(backgroundColor: GBABackgroundColor){
        switch backgroundColor {
        case .clear:
            self.backgroundColor = .clear
        }
    }
    
    func apply(gradientLayer: GBAGradientLayer){
        switch gradientLayer {
        case .shadedBlueGreen:
            self.apply(backgroundColor: .clear)
            
            let layer = CAGradientLayer()
                .set(frame: self.frame)
                .set(colors: [.primaryBlueGreen, .secondaryBlueGreen])
                .set(start: CGPoint(x: 1, y: 0))
                .set(end: CGPoint(x: 0.0, y: 1))
                .set(locations: [0.0, 0.5])
            
            self.layer.insertSublayer(layer, at: 0)
        }
    }
    
    func applyCornerRadius(_ radius: CGFloat? = nil){
        self.layer.cornerRadius = radius ?? 5
        self.layoutIfNeeded()
    }
    
    @discardableResult
    open override func resignFirstResponder() -> Bool {
        super.resignFirstResponder()
        self.subviews.forEach{ $0.resignFirstResponder() }
        return true
    }
}
