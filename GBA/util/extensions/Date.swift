



import UIKit

extension Date{
    
    static var Now: DateComponents{
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second, .weekday], from: date)
        return components
    }
    
    func getString(gbaDate: GBAFormat.Date)->String{
        return gbaDate.formatter.string(from: self)
    }
    
}

extension DateComponents{
    var weekDayName: String?{
        guard let day = weekday else { fatalError("No value found for weekday") }
        
        switch day {
        case 1:
            return "Sunday"
        case 2:
            return "Monday"
        case 3:
            return "Tuesday"
        case 4:
            return "Wednesday"
        case 5:
            return "Thursday"
        case 6:
            return "Friday"
        case 7:
            return "Saturday"
        default:
            return nil
        }
    }
    
    var weekDayAbv: String?{
        guard let day = weekday else { fatalError("No value found for weekday") }
        
        switch day {
        case 1:
            return "Sun"
        case 2:
            return "Mon"
        case 3:
            return "Tues"
        case 4:
            return "Wed"
        case 5:
            return "Thurs"
        case 6:
            return "Fri"
        case 7:
            return "Sat"
        default:
            return nil
        }
    }
    
    var monthName: String?{
        guard let month = month else { fatalError("No value found for weekday") }
        
        switch month {
        case 1:
            return "January"
        case 2:
            return "February"
        case 3:
            return "March"
        case 4:
            return "April"
        case 5:
            return "May"
        case 6:
            return "June"
        case 7:
            return "July"
        case 8:
            return "August"
        case 9:
            return "September"
        case 10:
            return "October"
        case 11:
            return "November"
        case 12:
            return "December"
        default:
            return nil
        }
    }
    
    var monthAbv: String?{
        guard let month = month else { fatalError("No value found for weekday") }
        
        switch month {
        case 1:
            return "Jan"
        case 2:
            return "Feb"
        case 3:
            return "Mar"
        case 4:
            return "April"
        case 5:
            return "May"
        case 6:
            return "June"
        case 7:
            return "July"
        case 8:
            return "Aug"
        case 9:
            return "Sept"
        case 10:
            return "Oct"
        case 11:
            return "Nov"
        case 12:
            return "Dec"
        default:
            return nil
        }
    }
}

fileprivate extension Int{
    
}
