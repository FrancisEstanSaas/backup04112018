//
//  GBAFont.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/24/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

struct GBAText{
    enum Size: CGFloat{
        case title = 24
        case header = 16
        case subContent = 14
        case normalNavbarIcon = 25
        case largerNavbarIcon = 44
    }
    
    enum Font{
        case main(CGFloat)
        
        var rawValue: UIFont{
            switch self {
            case let .main(size):
                return UIFont(name: "Helvetica Neue", size: size)!
            }
        }
    }
    
    enum ContentType{
        case Firstname
        
        var regEx: String{
            switch self {
            case .Firstname:
                return ""
            }
        }
        
        var placeholder: String{
            switch self {
            case .Firstname:
                return "First name"
            }
        }
    }
}
