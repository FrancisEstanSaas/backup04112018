//
//  GBAFormat.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/24/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class GBAFormat{
    enum Date: String{
        case ddMMyyyy = "dd-MM-yyyy"
        
        var formatter: DateFormatter{
            let format = DateFormatter()
            format.dateFormat = self.rawValue
            return format
        }
    }
}
