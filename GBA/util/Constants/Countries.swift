//
//  Countries.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//


enum Countries{
    case Philippines
    case America
    
    init(country code: String){
        switch code{
        case "PH":
            self = .Philippines
        case "USA":
            self = .America
        default:
            fatalError("country code not recognized.")
        }
    }
    
    var id: String{
        switch self{
        case .Philippines: return "PH"
        case .America: return "USA"
        }
    }
    
    var name: String{
        switch self {
        case .Philippines: return "Philippines"
        case .America: return "United States of America"
        }
    }
    
    var code: String{
        switch self{
        case .Philippines: return "PHL"
        case .America: return "USA"
        }
    }
    
    var currency: String{
        switch self{
        case .Philippines: return "PHP"
        case .America: return "USD"
        }
    }
    
    var ndd:[String]{
        switch self{
        case .Philippines: return ["63"]
        case .America: return ["1"]
        }
    }
    
    var timeZone: String{
        switch self{
        case .Philippines: return "Asia/Manila"
        case .America: return "UTC"
        }
    }
}
