//
//  typealiases.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 10/23/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

typealias ViewControllerIdentifier = String
typealias JSON = [String: Any]
typealias Header = [String: String]
